from django.conf.urls import url
from django.contrib import admin
from pokerApp.views import admin_view, table_view, table_list_view

urlpatterns = [
  url(r'^backend/', admin.site.urls),
  url(r'^table/', table_view.TableView.as_view(), name='table'),
  url(r'^admin/', admin_view.AdminView.as_view()),
  url(r'^$', table_list_view.TableListView.as_view(), name='table_list'),
]
