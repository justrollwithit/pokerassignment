from pokerApp.commands.command import Command
from pokerApp.models import User
from abc import abstractmethod


class UserCommand(Command):
    """
    A Command that may only be run by a User, not the admin
    """
    def __init__(self, username, args=None):
        super().__init__(username, args)
        try:
            self.user = User.objects.get(name=username)
        except User.DoesNotExist:
            self.user = None

    def getUser(self):
        """
        Obtains the User from their username. If the username is admin or there isn't a user with that name, the method
        returns None
        :rtype: User
        """
        return self.user

    @abstractmethod
    def run(self):
        """
        Validates that the command was called by a valid User, before executing the command. This method should be
        called from a derived class prior to its overridden functionality

        :return: nn error string, when the command wasn't called by a valid user, with information about what went wrong
        """
        if super().wasAdminCalled():
            return "The admin can't run this command"
        if self.getUser() is None:
            return f"No user found with name {self.username}"
