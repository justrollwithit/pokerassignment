from pokerApp.commands.user_command import UserCommand
from pokerApp.commands.validate_args import ValidateArgs
from pokerApp.game_objects.gamepool import game_pool
from pokerApp.game_objects.player import Player
from pokerApp.models import User


class JoinGameCommand(UserCommand, ValidateArgs):
    """
    A Command that may only be run by a User, to join a game
    """
    def areArgsValid(self):
        """
        This method is inherited by the ValidateArgs class and implemented to check that
        the given arguments to the command are valid, as well as the user.
        Returns strings if invalid input, otherwise returns True.
        """
        if super().getUser() is None:
            return "This is not a  valid user"
        if self.args is not None:
            return "This function does not take arguments."
        return True

    def run(self):
        """
        This method is inherited by the UserCommand class and implemented to run the given command.
        Returns strings if the string '{user} Joined Game'
        """
        check = self.areArgsValid()
        if not type(check) == bool:
            return check

        game_pool.join_game(Player(User.objects.get(name=self.username)))
        return f"{self.username} Joined Game"
