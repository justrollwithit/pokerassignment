import abc


class Command(abc.ABC):
    """
    An abstract class that represents a relation between a username and optional arguments and
    some function (executable action)
    """

    def __init__(self, username, args=None):
        """
        :param username: the name of the user who called the command
        :param args: a list of strings, each being an argument to the Command, or None if no arguments are provided
        :type username: str
        :type args: list[str]
        """
        if type(username) != str:
            raise TypeError("Command username must be a string")
        if username.find(" ") >= 0 or username.find("\t") >= 0 or username.find("\n") >= 0:
            raise ValueError("Username cannot contain whitespace")
        if args is not None and (type(args) != list or not all(type(a) == str for a in args)):
            raise TypeError("Command args must be None or a list of strings")

        self.username = username
        self.args = args
        super().__init__()

    def wasAdminCalled(self):
        """
        :return: if the command was called by the admin
        :rtype: bool
        """
        return self.username == "admin"

    @abc.abstractmethod
    def run(self):
        """
        This method should check any arbitrary preconditions. If any are not met, an error string should be returned.
        Otherwise, an arbitrary
        function/action should be executed. The result of the command should be returned in the form of a string
        message.

        :return: a string that is the result message of running the command
        :rtype: str
        """
        pass
