from pokerApp.commands.admin_command import AdminCommand
from pokerApp.commands.validate_args import ValidateArgs
from pokerApp.models import User


class AddUserCommand(AdminCommand, ValidateArgs):
    """
    A Command that may only be run by a Admin, to add a user to database
    """
    def areArgsValid(self):
        """
        This method is inherited by the ValidateArgs class and implemented to check that
        the given arguments to the command are valid, as well as the user.
        Returns strings if invalid input, otherwise returns True.
        """
        if self.args is None:
            return "Please enter a username"
        elif len(self.args) > 1:
            return "Usernames cannot contain whitespace"

        if User.exists(self.args[0]):
            return "Player can't have duplicate names"
        else:
            return True

    def run(self):
        """
        This method is inherited by the UserCommand class and implemented to run the given command.
        Returns strings if the string 'USer {user} was successfully created'
        """
        if super().run() is not None:
            return super().run()
        check = self.areArgsValid()
        if check is not True:
            return check
        user = User.create(self.args[0])
        user.save()
        return f"User {self.args[0]} was successfully created"
