from pokerApp.commands.command import Command
from abc import abstractmethod


class AdminCommand(Command):
    """
    A Command that may only be run by the admin.
    """
    @abstractmethod
    def run(self):
        """
        This method should be called prior to the overridden functionality of a derived class. It simply checks that
        the command was called by the admin

        :return: An error string when command is not called by the admin
        """
        if not super().wasAdminCalled():
            return "This command can only be used by the admin"
