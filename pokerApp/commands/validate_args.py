import abc


class ValidateArgs(abc.ABC):
    """
    This interface defines a class as having a list of arguments (as an instance variable) that must meet arbitrary
    conditions to use those arguments
    """
    @abc.abstractmethod
    def areArgsValid(self):
        """
        This method should check a list instance variable for certain conditions about its elements
        :return: True if the arguments list meets arbitrary conditions, otherwise, it returns a string explaining what
        condition(s) weren't met
        """
        pass
