from pokerApp.commands.admin_command import AdminCommand
from pokerApp.commands.validate_args import ValidateArgs
from pokerApp.game_objects.gamepool import game_pool


class PlayRoundCommand(AdminCommand, ValidateArgs):
    def areArgsValid(self):
        if self.args is not None:
            return "This command does not take arguments."
        return True

    def run(self):
        if super().run() is not None:
            return super().run()
        check = self.areArgsValid()
        if not type(check) == bool:
            return check
        games = game_pool.get_game_list()
        return games[0].play_round()