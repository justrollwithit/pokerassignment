from pokerApp.commands.playround import PlayRoundCommand
from pokerApp.models import User
from pokerApp.commands.adduser import AddUserCommand
from pokerApp.commands.joingame import JoinGameCommand


class CommandRunner:
    """
    A helper class for converting a string into command parts, and running a Command based upon the values of those
    parts.
    """

    @staticmethod
    def split_command(command_string=":"):
        """
        Returns a list of the 3 parts of a command string

        :param command_string: a string representation of a command in the form <username>:<command> <args>
        :type command_string: str
        :return: a list with the form [<username>, <command>, <args>], None means that the input string didn't contain
        that command part, if the entire string is invalid None is returned rather than a list
        """
        username_sep = command_string.find(":")  # Index of the colon between username and command name
        if username_sep < 0: # Invalid command string
            return None
        # Index of the space between the command name and args or -1
        args_sep = command_string.find(" ")

        username = None
        if username_sep > 0:  # username is before a ':' char
            username = command_string[0:username_sep]

        # command name is after the ':' and ends at a ' ', if one is present
        command_name = command_string[username_sep + 1::]

        args = None
        if args_sep != -1:  # when args are provided
            command_name = command_string[username_sep + 1:args_sep]  # remove args from the command name
            args = command_string[args_sep + 1::].split(" ")

        # command string was missing the command name
        if len(command_name) == 0:
            command_name = None

        return [username, command_name, args]

    @staticmethod
    def run(username, command_name, args):
        """
        Runs concrete derived instances of Command that are assumed to have fully implemented any abstract methods. The
        commands to run must be determined within this method's implementation

        :param username: the name of the calling user
        :type username: str | None
        :param command_name: the text name of the command used by the end user
        :type command_name: str | None
        :param args: list of arguments
        :type args: list[str] | None
        :return: A result message or error message
        :rtype: str
        """
        if username is None:
            return "Please enter a username"
        elif username.find(" ") >= 0 or username.find("\t") >= 0 or username.find("\n") >= 0:
            return "Usernames cannot contain whitespace (spaces, tabs, new lines)"
        elif username != "admin" and not User.exists(username):
            return "There isn't a user with that username"

        if command_name is None:
            return "Please enter a command"

        # Run commands based on the command name
        if command_name == "adduser":
            return AddUserCommand(username, args).run()
        elif command_name == "joingame":
            return JoinGameCommand(username, args).run()
        elif command_name == 'playround':
            return PlayRoundCommand(username, args).run()
        else:  # No command with provided command name
            return "No such command"
