from django.test import TestCase
from pokerApp.models import User
from pokerApp.commands.user_command import UserCommand


class MockUserCommand(UserCommand):
    def run(self):
        pre_check = super().run()
        if pre_check is not None:
            return pre_check
        else:
            return "Done"


class GetUser(TestCase):
    def setUp(self):
        self.valid_user = User.objects.create(name="john")
        self.valid_user.save()

        self.admin_called = MockUserCommand("admin")
        self.valid_user_called = MockUserCommand(self.valid_user.name)
        self.invalid_user_called = MockUserCommand("user")

    def test_admin_is_none(self):
        self.assertIsNone(self.admin_called.getUser())

    def test_valid_user(self):
        self.assertEqual(self.valid_user_called.getUser(), self.valid_user)

    def test_invalid_user(self):
        self.assertIsNone(self.invalid_user_called.getUser())


class RunUserCommand(TestCase):
    def setUp(self):
        self.valid_user = User.objects.create(name="john")
        self.invalid_username = "user"

        self.admin_called = MockUserCommand("admin")
        self.valid_user_called = MockUserCommand("john")
        self.invalid_user_called = MockUserCommand(self.invalid_username)

    def test_prevent_admin_run(self):
        self.assertEqual("The admin can't run this command", self.admin_called.run(), )

    def test_valid_user_run(self):
        self.assertEqual("Done", self.valid_user_called.run())

    def test_invalid_user_run(self):
        self.assertEqual(f"No user found with name {self.invalid_username}", self.invalid_user_called.run())
