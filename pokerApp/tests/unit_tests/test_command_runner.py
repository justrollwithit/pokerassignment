from django.test import TestCase
from pokerApp.commands.command_runner import CommandRunner


class SplitCommand(TestCase):
    def setUp(self):
        self.valid_command_str1 = "user:command"
        self.valid_command_str2 = "admin:acommand a b c"
        self.no_username_str1 = ":command"
        self.no_username_str2 = ":command a"
        self.no_command_str = "user: a"
        self.no_user_and_command_str = ": a"
        self.no_user_command_and_args_str = ":"
        self.invalid_command_str = "bad command string"

    def test_valid_command_strings(self):
        self.assertEqual(["user", "command", None], CommandRunner.split_command(self.valid_command_str1))
        self.assertEqual(["admin", "acommand", ["a", "b", "c"]], CommandRunner.split_command(self.valid_command_str2))

    def test_no_username_strings(self):
        self.assertEqual([None, "command", None], CommandRunner.split_command(self.no_username_str1))
        self.assertEqual([None, "command", ["a"]], CommandRunner.split_command(self.no_username_str2))

    def test_no_command_string(self):
        self.assertEqual(["user", None, ["a"]], CommandRunner.split_command(self.no_command_str))

    def test_only_args_string(self):
        self.assertEqual([None, None, ["a"]], CommandRunner.split_command(self.no_user_and_command_str))

    def test_valid_no_username_command_and_args_string(self):
        self.assertEqual([None, None, None], CommandRunner.split_command(self.no_user_command_and_args_str))
        self.assertEqual([None, None, None], CommandRunner.split_command())

    def test_invalid_command_string(self):
        self.assertEqual(None, CommandRunner.split_command(self.invalid_command_str))
