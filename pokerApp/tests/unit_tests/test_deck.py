from django.test import TestCase
from unittest import mock
import pokerApp.game_objects.card
from pokerApp.game_objects.card import Card
from pokerApp.game_objects.deck import Deck


# deck tests
class DeckInit(TestCase):
    """@mock.patch('card.Card') replaces any instance of the Card class with
        a MagicMock"""
    def setUp(self):
        self.cards = []
        for s in range(1, 5):
            for r in range(1, 14):
                mock_card = mock.Mock(pokerApp.game_objects.card.Card)
                mock_card.getSuit = s
                mock_card.getRank = r
                self.cards.append(mock_card)

    def test_standard_deck(self):
        deck = Deck()
        self.assertEqual(deck.count(), 52)
        for s in range(4, 0, -1):
            for r in range(13, 0, -1):
                mock_card = self.cards.pop()
                card = deck.draw()
                self.assertEqual(mock_card.getSuit, card.getSuit())
                self.assertEqual(mock_card.getRank, card.getRank())


class DeckCount(TestCase):
    """
    drawing card from the deck and testing if each removal matches the count method return
    """

    def setUp(self):
        self.d = Deck()

    def test_deck_count(self):
        for i in range(52):
            self.assertEqual(self.d.count(), 52 - i)
            self.d.draw()


class DeckDrawValue(TestCase):
    """
    Tests that a non-empty Deck can draw cards
    """

    # Tests that only Card objects are drawn from a Deck
    def test_draws_cards(self):
        deck = Deck()
        for i in range(1, 53):
            with self.subTest(f"Draw {i}"):
                self.assertIsInstance(deck.draw(), Card)


class DeckDrawException(TestCase):
    """
    this test tests drawing from an empty deck.
    we start by initializing a deck (d) and drawing
    52 cards from it. Then we attempt to draw 1 card
    from the now empty deck which should raise a
    ValueError
    """

    def test_empty_deck(self):
        d = Deck()
        for x in range(52):
            d.draw()
        with self.assertRaises(ValueError):
            d.draw()


class DeckShuffle(TestCase):
    """
    assume that a deck of cards is initialized as 1 - 13 rank all in suit 1, followed by suits 2 - 4
    tests every card in a deck to see if the deck is shuffled
    test an empty deck shuffle throws a TypeError
    """

    def setUp(self):
        self.cards = []
        # iterate through each suit
        for s in range(1, 5):
            # iterate through each rank within suit
            for r in range(1, 14):
                # create a mock card with mock card functions
                fake_card = mock.Mock(pokerApp.game_objects.card.Card)
                fake_card.getSuit = mock.Mock(value=s)
                fake_card.getRank = mock.Mock(value=r)
                # add mock card to cards
                self.cards.append(fake_card)

    @mock.patch('pokerApp.game_objects.card.Card')
    def test_shuffle(self, mock_card):
        self.d = Deck()
        # shuffle d
        self.d.shuffle()
        # init count to 0 for count how many cards in same place
        count = 0
        # iterate through all cards in list of cards
        for c in self.cards:
            # take top card and make a mock instance of it
            self.mock_card = pokerApp.game_objects.card.Card(self.d.draw())
            # evaluate if card rank and suit equivalent to cards in list
            if c.getSuit.value == mock_card.getSuit() and c.getRank.value == mock_card.getRank():
                # increment count when match found
                count += 1

        # checks if count < 52
        self.assertLess(count, 52)

    def test_empty_shuffle(self):
        self.d = Deck()
        # empty deck
        for i in range(52):
            self.d.draw()
        # assert type error for empty deck when shuffled
        with self.assertRaises(TypeError):
            self.d.shuffle()


class DeckIsEmpty(TestCase):

    def test_not_empty(self):
        self.deck = Deck()
        self.assertFalse(self.deck.is_empty())

    def test_empty(self):
        self.deck = Deck()
        for draw in range(52):
            self.deck.draw()
        self.assertTrue(self.deck.is_empty())