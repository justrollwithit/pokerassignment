from django.test import TestCase
from pokerApp.commands.admin_command import AdminCommand

"""
As this is testing an abstract class, there is less to test, as implementation is assumed to follow provided 
documentation/requirements
"""


class MockGoodAdminCommand(AdminCommand):
    """
    An AdminCommand that uses the AdminBase class run method result prior to running any command specific code
    """
    def run(self):
        pre_check = super().run()
        if pre_check is not True:
            return pre_check
        else:
            return "Done"


class CheckedCallerIsAdmin(TestCase):
    def setUp(self):
        self.admin_called = MockGoodAdminCommand("admin")
        self.user_called = MockGoodAdminCommand("john")

    def test_admin_command_ran(self):
        self.assertNotEqual(self.admin_called.run(), "This command can only be used by the admin")

    def test_prevented_user_run(self):
        self.assertEqual(self.user_called.run(), "This command can only be used by the admin")

