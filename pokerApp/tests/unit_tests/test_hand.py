from unittest.mock import Mock
from django.test import TestCase
from pokerApp.game_objects.card import Card
from pokerApp.game_objects.hand import Hand
from pokerApp.game_objects.poker_hand_types import PokerHandTypes


class HandInit(TestCase):
    def setUp(self):
        self.hand = Hand()
        self.hand_score = 0

    def test_hand_init(self):
        self.assertEqual(self.hand.get_hand_size(), 0)


class HandGetSize(TestCase):
    def setUp(self):
        self.hand_size_one = Hand()
        self.hand_size_two = Hand()
        self.hand_size_five = Hand()
        self.mock_card = Mock(spec=Card)

        for i in range(1):
            self.hand_size_one.add_card(self.mock_card)
        for i in range(2):
            self.hand_size_two.add_card(self.mock_card)
        for i in range(5):
            self.hand_size_five.add_card(self.mock_card)

    def test_size_one(self):
        self.assertEqual(self.hand_size_one.get_hand_size(), 1)

    def test_size_two(self):
        self.assertEqual(self.hand_size_two.get_hand_size(), 2)

    def test_size_five(self):
        self.assertEqual(self.hand_size_five.get_hand_size(), 5)


class HandAddCard(TestCase):
    def setUp(self):
        self.hand = Hand()
        self.mock_card = Mock(spec=Card)

    def test_add_one_card(self):
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.get_hand_size(), 1)

    def test_add_two_cards(self):
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.get_hand_size(), 2)

    def test_add_five_cards(self):
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.get_hand_size(), 5)

    def test_add_six_cards(self):
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.add_card(self.mock_card), self.mock_card)
        self.assertEqual(self.hand.get_hand_size(), 6)


class HandAddCardRaiseError(TestCase):
    def setUp(self):
        self.hand = Hand()
        self.mock_card = Mock(spec=Card)
        self.str_type = "not a card"
        self.int_type = 1

    def test_add_raise_type_error(self):
        with self.assertRaises(TypeError):
            self.hand.add_card(self.str_type)
        with self.assertRaises(TypeError):
            self.hand.add_card(self.int_type)


class HandGetCards(TestCase):
    def setUp(self):
        self.hand_one_card = Hand()
        self.hand_two_cards = Hand()
        self.hand_five_cards = Hand()
        self.hand_six_cards = Hand()
        self.mock_card = Mock(spec=Card)
        self.mock_card.getSuit = Mock(return_value=1)
        self.mock_card.getRank = Mock(return_value=1)
        self.one_card = []
        self.two_cards = []
        self.five_cards = []
        self.six_cards = []

        for i in range(1):
            self.hand_one_card.add_card(self.mock_card)
            self.one_card.append(self.mock_card)
        for i in range(2):
            self.hand_two_cards.add_card(self.mock_card)
            self.two_cards.append(self.mock_card)
        for i in range(5):
            self.hand_five_cards.add_card(self.mock_card)
            self.five_cards.append(self.mock_card)
        for i in range(6):
            self.hand_six_cards.add_card(self.mock_card)
            self.six_cards.append(self.mock_card)

    def test_one_card_hand(self):
        for card in self.hand_one_card.get_cards():
            self.assertEqual(card.getRank(), self.mock_card.getRank())
            self.assertEqual(card.getSuit(), self.mock_card.getSuit())

    def test_two_card_hand(self):
        for card in self.hand_two_cards.get_cards():
            self.assertEqual(card.getRank(), self.mock_card.getRank())
            self.assertEqual(card.getSuit(), self.mock_card.getSuit())

    def test_five_card_hand(self):
        for card in self.hand_five_cards.get_cards():
            self.assertEqual(card.getRank(), self.mock_card.getRank())
            self.assertEqual(card.getSuit(), self.mock_card.getSuit())

    def test_six_card_hand(self):
        for card in self.hand_six_cards.get_cards():
            self.assertEqual(card.getRank(), self.mock_card.getRank())
            self.assertEqual(card.getSuit(), self.mock_card.getSuit())


class HandSetHandScore(TestCase):
    def setUp(self):
        self.hand = Hand()
        self.hand_score1 = PokerHandTypes.DEFAULT
        self.hand_score2 = PokerHandTypes.THREE_OF_KIND
        self.hand_score3 = PokerHandTypes.FULL_HOUSE
        self.hand_score4 = PokerHandTypes.FOUR_OF_KIND
        self.hand_score5 = PokerHandTypes.STRAIGHT_FLUSH

    def test_set_hand_score1(self):
        self.assertEqual(self.hand_score1, self.hand.get_hand_score())
        self.hand.set_hand_score(self.hand_score1)
        self.assertEqual(self.hand_score1, self.hand.get_hand_score())

    def test_set_hand_score2(self):
        self.assertEqual(self.hand_score1, self.hand.get_hand_score())
        self.hand.set_hand_score(self.hand_score2)
        self.assertEqual(self.hand_score2, self.hand.get_hand_score())

    def test_set_hand_score3(self):
        self.assertEqual(self.hand_score1, self.hand.get_hand_score())
        self.hand.set_hand_score(self.hand_score3)
        self.assertEqual(self.hand_score3, self.hand.get_hand_score())

    def test_set_hand_score4(self):
        self.assertEqual(self.hand_score1, self.hand.get_hand_score())
        self.hand.set_hand_score(self.hand_score4)
        self.assertEqual(self.hand_score4, self.hand.get_hand_score())

    def test_set_hand_score5(self):
        self.assertEqual(self.hand_score1, self.hand.get_hand_score())
        self.hand.set_hand_score(self.hand_score5)
        self.assertEqual(self.hand_score5, self.hand.get_hand_score())


class HandGetHandScore(TestCase):
    def setUp(self):
        self.hand = Hand()
        self.hand_score1 = PokerHandTypes.DEFAULT
        self.hand_score2 = PokerHandTypes.TWO_PAIR
        self.hand_score3 = PokerHandTypes.STRAIGHT
        self.hand_score4 = PokerHandTypes.FLUSH
        self.hand_score5 = PokerHandTypes.FOUR_OF_KIND

    def test_set_hand_score1(self):
        self.hand.set_hand_score(self.hand_score1)
        self.assertEqual(self.hand_score1, self.hand.get_hand_score())

    def test_set_hand_score2(self):
        self.hand.set_hand_score(self.hand_score2)
        self.assertEqual(self.hand_score2, self.hand.get_hand_score())

    def test_set_hand_score3(self):
        self.hand.set_hand_score(self.hand_score3)
        self.assertEqual(self.hand_score3, self.hand.get_hand_score())

    def test_set_hand_score4(self):
        self.hand.set_hand_score(self.hand_score4)
        self.assertEqual(self.hand_score4, self.hand.get_hand_score())

    def test_set_hand_score5(self):
        self.hand.set_hand_score(self.hand_score5)
        self.assertEqual(self.hand_score5, self.hand.get_hand_score())


class HandStr(TestCase):
    def setUp(self):
        self.hand1 = Hand()
        self.hand2 = Hand()
        self.hand3 = Hand()

        self.hand_str1 = 'AC AS AH AD 2C 2S 2H 2D 3C 3S 3H 3D 4C 4S 4H 4D'
        self.hand_str2 = '5C 5S 5H 5D 6C 6S 6H 6D 7C 7S 7H 7D 8C 8S 8H 8D'
        self.hand_str3 = '9C 9S 9H 9D 10C 10S 10H 10D JC JS JH JD QC QS QH QD KC KS KH KD'

        suits = ['C', 'S', 'H', 'D']
        ranks = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']

        for rank in range(0, 4):
            for suit in suits:
                mock_card = Mock(spec=Card)
                mock_card.__str__ = Mock(return_value=ranks[rank] + suit)
                self.hand1.add_card(mock_card)

        for rank in range(4, 8):
            for suit in suits:
                mock_card = Mock(spec=Card)
                mock_card.__str__ = Mock(return_value=ranks[rank] + suit)
                self.hand2.add_card(mock_card)

        for rank in range(8, 13):
            for suit in suits:
                mock_card = Mock(spec=Card)
                mock_card.__str__ = Mock(return_value=ranks[rank] + suit)
                self.hand3.add_card(mock_card)

    def test_hand_str_1(self):
        self.assertEqual(self.hand_str1, self.hand1.__str__())

    def test_hand_str_2(self):
        self.assertEqual(self.hand_str2, self.hand2.__str__())

    def test_hand_str_3(self):
        self.assertEqual(self.hand_str2, self.hand2.__str__())

