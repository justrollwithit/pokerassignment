from django.test import TestCase
from unittest.mock import Mock
from pokerApp.game_objects.gamepool import GamePool
from pokerApp.game_objects.five_card_stud import FiveCardStud
from pokerApp.game_objects.player import Player


class GamePoolInit(TestCase):
    def setUp(self):
        self.g = GamePool()

    def test_gamepool_init(self):
        self.assertEqual(self.g.get_current_index(), 0)
        self.assertEqual(len(self.g.get_game_list()), 1)

class GetGameList(TestCase):
    def setUp(self):
        self.g = GamePool()

    def test_get_game_list(self):
        self.assertIsInstance(self.g.get_game_list(), list)

class GetCurrentIndex(TestCase):
    def setUp(self):
        self.g = GamePool()

    def test_get_current_index(self):
        self.assertEqual(self.g.get_current_index(), 0)

class RemoveGame(TestCase):
    def setUp(self):
        self.g = GamePool()
        self.mock_game = Mock(spec=FiveCardStud)
        for i in range (10):
            self.g.get_game_list().append(self.mock_game)
    def test_remove_game(self):
        for i in range (0, 5):
            curlength = len(self.g.get_game_list())
            retGame = self.g.remove_game(0)
            self.assertEqual(len(self.g.get_game_list()), curlength)
            self.assertIsInstance(retGame, FiveCardStud)
            retGame = self.g.remove_game(curlength - 1)
            self.assertEqual(len(self.g.get_game_list()), curlength - 1)
            self.assertIsInstance(retGame, FiveCardStud)

class JoinGame(TestCase):
    def setUp(self):
        self.g = GamePool()
        self.mock_player = Mock(spec=Player)
        self.mock_player.get_hand_score = Mock(return_value=3)
        self.mock_player.get_hand_score()

    def test_join_game(self):
        self.assertEqual(self.g.join_game(self.mock_player), self.mock_player)