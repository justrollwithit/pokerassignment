from django.test import TestCase
from unittest.mock import Mock
from pokerApp.commands.command import Command

"""
As this is testing an abstract class, there is less to test, as implementation is assumed to follow provided 
documentation/requirements
"""


class MockCommand(Command):
    def run(self):
        return "Command result"


class ConstructCommandUsername(TestCase):
    def setUp(self):
        self.valid_username = "john"
        self.admin_username = "admin"
        self.invalid_username1 = 4  # username must be a string
        self.invalid_username2 = "john doe"  # username cannot contain whitespace

    def test_valid_username(self):
        self.assertIsInstance(MockCommand(self.valid_username), Command)
        self.assertIsInstance(MockCommand(self.admin_username), Command)

    def test_invalid_username(self):
        with self.assertRaises(TypeError):
            MockCommand(self.invalid_username1)
        with self.assertRaises(ValueError):
            MockCommand(self.invalid_username2)


class ConstructCommandArgs(TestCase):
    def setUp(self):
        self.username = "john"
        self.valid_args1 = None  # None represents no arguments
        self.valid_args2 = ["arg1", "arg2"]  # args must be a list of strings
        self.invalid_args1 = 3.5
        self.invalid_args2 = ["arg1", 9]

    def test_valid_args(self):
        self.assertIsInstance(MockCommand(self.username, self.valid_args1), Command)
        self.assertIsInstance(MockCommand(self.username, self.valid_args2), Command)

    def test_invalid_args(self):
        with self.assertRaises(TypeError):
            MockCommand(self.username, self.invalid_args1)
        with self.assertRaises(TypeError):
            MockCommand(self.username, self.invalid_args2)


class CommandCalledByAdmin(TestCase):
    def setUp(self):
        self.username = "john"
        self.admin = "admin"

        self.user_called_command = MockCommand(self.username)
        self.admin_called_command = MockCommand(self.admin)

    def test_called_by_user(self):
        self.assertFalse(self.user_called_command.wasAdminCalled())

    def test_called_by_admin(self):
        self.assertTrue(self.admin_called_command.wasAdminCalled())


