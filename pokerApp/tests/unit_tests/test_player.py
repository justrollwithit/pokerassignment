from unittest.mock import Mock
from django.test import TestCase
from pokerApp.game_objects.hand import Hand
from pokerApp.game_objects.player import Player
from pokerApp.models import User


class PlayerInitCorrectUser(TestCase):
    def setUp(self):
        self.username = 'player'
        self.high_score = 0
        self.chips = 100
        self.mock_user = Mock(spec=User)
        self.mock_user.name = self.username
        self.mock_user.high_score = self.high_score
        self.player = Player(self.mock_user)
        self.hand_score = 0

    def test_valid_player(self):
        self.assertEqual(self.player.user.name, self.username)
        self.assertEqual(self.player.user.high_score, self.high_score)
        self.assertEqual(self.chips, self.player.chips)
        self.assertEqual(self.username, self.player.user.name)
        self.assertEqual(self.high_score, self.player.user.high_score)
        

class PlayerInitRaiseError(TestCase):
    def setUp(self):
        self.str_type = 'wrong type'
        self.int_type = '1'

    def test_raise_type_error(self):
        with self.assertRaises(TypeError):
            Player(self.str_type)
        with self.assertRaises(TypeError):
            Player(self.int_type)


class PlayerGetHand(TestCase):
    def setUp(self):
        self.player = Player(Mock(spec=User))

    def test_get_hand(self):
        self.assertTrue(isinstance(self.player.get_hand(), Hand))


class PlayerBet(TestCase):
    def setup(self):
        self.player = Player(Mock(spec=User))

    def test_bet_less(self):
        self.setup()
        self.assertTrue(self.player.bet(10))
        self.assertTrue(self.player.bet(20))
        self.assertTrue(self.player.bet(30))

    def test_bet_equal(self):
        self.setup()
        self.assertTrue(self.player.bet(100))

    def test_bet_over(self):
        self.setup()
        self.assertFalse(self.player.bet(101))
        self.assertFalse(self.player.bet(200))

    def test_bet_TypeError(self):
        self.setup()
        with self.assertRaises(TypeError):
            self.player.bet(True)
        with self.assertRaises(TypeError):
            self.player.bet("string")
        with self.assertRaises(TypeError):
            self.player.bet([1, 2])
        with self.assertRaises(TypeError):
            self.player.bet(1.2)


class PlayerStr(TestCase):
    def setUp(self):
        self.mock_user1 = Mock(spec=User)
        self.mock_user1.name = "player1"
        self.player1 = Player(self.mock_user1)

        self.mock_user2 = Mock(spec=User)
        self.mock_user2.name = "user"
        self.player2 = Player(self.mock_user2)

        self.mock_user3 = Mock(spec=User)
        self.mock_user3.name = "john"
        self.player3 = Player(self.mock_user3)

    def test_player_str_1(self):
        self.assertEqual(self.mock_user1.name, self.player1.__str__())

    def test_player_str_2(self):
        self.assertEqual(self.mock_user2.name, self.player2.__str__())

    def test_player_str_3(self):
        self.assertEqual(self.mock_user3.name, self.player3.__str__())
