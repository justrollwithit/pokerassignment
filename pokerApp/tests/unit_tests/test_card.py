from django.test import TestCase
from pokerApp.game_objects.card import Card


# Card tests
class CardInitCorrectRankAndSuit(TestCase):
    def test_valid_suit_rank(self):
        for i in range(1, 5):
            for j in range(1, 14):
                card = Card(i, j)
                self.assertTrue(card.getSuit() == i)
                self.assertTrue(card.getRank() == j)


class CardInitRaiseError(TestCase):
    def setUp(self):
        self.str_type = 'wrong type'
        self.double_type = 1.1
        self.valid_rank = 1
        self.valid_suit = 1
        self.invalid_suit_rank_low = 0
        self.invalid_suit_high = 5
        self.invalid_rank_high = 14

    def test_raise_value_error_suit(self):
        with self.assertRaises(ValueError):
            Card(self.invalid_suit_rank_low, self.valid_rank)
        with self.assertRaises(ValueError):
            Card(self.invalid_suit_high, self.valid_rank)

    def test_raise_type_error_suit(self):
        with self.assertRaises(TypeError):
            Card(self.str_type, self.valid_rank)
        with self.assertRaises(TypeError):
            Card(self.double_type, self.valid_rank)

    def test_raise_value_error_rank(self):
        with self.assertRaises(ValueError):
            Card(self.valid_suit, self.invalid_suit_rank_low)
        with self.assertRaises(ValueError):
            Card(self.valid_suit, self.invalid_rank_high)

    def test_raise_type_error_rank(self):
        with self.assertRaises(TypeError):
            Card(self.valid_suit, self.str_type)
        with self.assertRaises(TypeError):
            Card(self.valid_suit, self.double_type)


class CardGetSuit(TestCase):
    """
    testing suit of every possible value to see if it returns the correct suit
    """

    def test_get_suit(self):
        for i in range(1, 5):
            for j in range(1, 14):
                c = Card(i, j)
                self.assertEqual(i, c.getSuit())


class CardGetRank(TestCase):

    def setUp(self):
        self.maxRank = 13
        self.maxSuit = 4
        self.minRank = self.minSuit = 1
        self.invalidRank = 20

    # test individual cases without loops
    def test_valid_rank01(self):
        card = Card(self.minRank, self.minSuit)
        self.assertEqual(self.minRank, card.getRank())

        card2 = Card(2, 3)
        self.assertEqual(3, card2.getRank())

    # test with only rank in loop
    def test_valid_rank02(self):
        suit = 3
        for rank in range(self.minRank, self.maxRank + 1):
            card = Card(suit, rank)
            self.assertEqual(rank, card.getRank())

    # test invalid rank
    def test_invalid_rank(self):
        with self.assertRaises(ValueError):
            card = Card(self.minSuit, self.invalidRank)
        with self.assertRaises(Exception):
            card.getRank()

    """ 
    this test tests all the valid suit/rank combinations
    and ensures that getRank returns the correct
    integer value of the rank 
    """

    def test_all_valid_ranks(self):
        for suit in range(self.minSuit, self.maxSuit + 1):
            for rank in range(self.minRank, self.maxRank + 1):
                card = Card(suit, rank)
                self.assertEqual(rank, card.getRank())


class CardGetValue(TestCase):
    """
    Checks that all possible 52 cards have the correct value.
    As no game is currently specified, we can assume any card has a value of 0.
    """

    # Initializes a list of Card objects, 1 for each suit and rank combination
    def setUp(self):
        self.cards = []

        for suit in range(1, 5):
            for rank in range(1, 14):
                self.cards.append(Card(suit, rank))

    def test_card_values(self):
        for card in self.cards:
            with self.subTest():
                self.assertEqual(0,
                                 card.getValue(),
                                 "Incorrect card value")


class CardStr(TestCase):
    """
    tests card.__str__() with every value 1-13 rank adn 1-4 suit
    """

    def setUp(self):
        self.valid_rank = 2
        self.valid_suit = 2
        self.invalid_rank = 1
        self.invalid_suit = 4
        self.card_str = '2S'
        self.cards = []
        for s in range(1, 5):  # all suits
            for r in range(1, 14):  # all ranks
                # creates sting for each card possible to check against __str__ function
                string = ""
                if r == 1:
                    string += "A"
                elif r == 11:
                    string += "J"
                elif r == 12:
                    string += "Q"
                elif r == 13:
                    string += "K"
                else:
                    string += f"{r}"
                if s == 1:
                    string += "C"
                elif s == 2:
                    string += "S"
                elif s == 3:
                    string += "H"
                else:
                    string += "D"
                self.cards.append(string)

                # assertion check on current card
                self.assertEquals(Card(s, r).__str__(), string)


class CardEq(TestCase):
    def setup(self):
        self.card1a = Card(1, 1)
        self.card1b = Card(1, 1)
        self.card2a = Card(2, 1)
        self.card2b = Card(2, 1)
        self.card3a = Card(1, 2)
        self.card3b = Card(1, 2)
        self.card4a = Card(2, 2)
        self.card4b = Card(2, 2)

    def test_card_eq(self):
        self.setup()
        self.assertEqual(True, self.card1a.__eq__(self.card1b))
        self.assertEqual(True, self.card2a.__eq__(self.card2b))
        self.assertEqual(True, self.card3a.__eq__(self.card3b))
        self.assertEqual(True, self.card4a.__eq__(self.card4b))

    def test_card_not_eq(self):
        self.setup()
        self.assertEqual(False, self.card1a.__eq__(self.card2a))
        self.assertEqual(False, self.card1a.__eq__(self.card3a))
        self.assertEqual(False, self.card1a.__eq__(self.card4a))

        self.assertEqual(False, self.card2a.__eq__(self.card1a))
        self.assertEqual(False, self.card2a.__eq__(self.card3a))
        self.assertEqual(False, self.card2a.__eq__(self.card4a))

        self.assertEqual(False, self.card3a.__eq__(self.card1a))
        self.assertEqual(False, self.card3a.__eq__(self.card2a))
        self.assertEqual(False, self.card3a.__eq__(self.card4a))

        self.assertEqual(False, self.card4a.__eq__(self.card1a))
        self.assertEqual(False, self.card4a.__eq__(self.card2a))
        self.assertEqual(False, self.card4a.__eq__(self.card3a))

class CardNe(TestCase):
    def setup(self):
        self.card1a = Card(1, 1)
        self.card1b = Card(1, 1)
        self.card2a = Card(2, 1)
        self.card2b = Card(2, 1)
        self.card3a = Card(1, 2)
        self.card3b = Card(1, 2)
        self.card4a = Card(2, 2)
        self.card4b = Card(2, 2)

    def test_card_ne(self):
        self.setup()
        self.assertEqual(True, self.card1a.__ne__(self.card2a))
        self.assertEqual(True, self.card1a.__ne__(self.card3a))
        self.assertEqual(True, self.card1a.__ne__(self.card4a))

        self.assertEqual(True, self.card2a.__ne__(self.card1a))
        self.assertEqual(True, self.card2a.__ne__(self.card3a))
        self.assertEqual(True, self.card2a.__ne__(self.card4a))

        self.assertEqual(True, self.card3a.__ne__(self.card1a))
        self.assertEqual(True, self.card3a.__ne__(self.card2a))
        self.assertEqual(True, self.card3a.__ne__(self.card4a))

        self.assertEqual(True, self.card4a.__ne__(self.card1a))
        self.assertEqual(True, self.card4a.__ne__(self.card2a))
        self.assertEqual(True, self.card4a.__ne__(self.card3a))

    def test_card_not_ne(self):
        self.setup()
        self.assertEqual(False, self.card1a.__ne__(self.card1b))
        self.assertEqual(False, self.card2a.__ne__(self.card2b))
        self.assertEqual(False, self.card3a.__ne__(self.card3b))
        self.assertEqual(False, self.card4a.__ne__(self.card4b))