from django.test import TestCase
from pokerApp.models import Table
from pokerApp.models import User
from pokerApp.models import Table


class TableModel(TestCase):
    def test_add_user(self):
        self.user = User.objects.create(name='bob')
        self.user1 = User.objects.create(name='bar')
        self.table = Table.objects.create(name='foo')
        self.table.save()
        self.table.add_user(self.table, self.user)
        self.table.add_user(self.table, self.user1)
        print(self.table)
        print(self.table.user_set.all())
