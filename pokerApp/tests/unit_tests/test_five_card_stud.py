from django.test import TestCase
from pokerApp.game_objects.card import Card
from pokerApp.game_objects.deck import Deck
from pokerApp.game_objects.five_card_stud import FiveCardStud
from pokerApp.game_objects.hand import Hand
from pokerApp.game_objects.player import Player
from unittest.mock import Mock

from pokerApp.game_objects.poker_hand_types import PokerHandTypes


class StudInit(TestCase):
    def setUp(self):
        self.game1_id = 0
        self.game2_id = 1
        self.game3_id = -1
        self.player_count = 0

        self.game1 = FiveCardStud(self.game1_id)
        self.game2 = FiveCardStud(self.game2_id)
        self.game3 = FiveCardStud(self.game3_id)

    def test_game1(self):
        self.assertEqual(self.player_count, self.game1.get_player_count())
        self.assertEqual(self.game1.get_game_id(), self.game1.get_game_id())

    def test_game2(self):
        self.assertEqual(self.player_count, self.game2.get_player_count())
        self.assertEqual(self.game2.get_game_id(), self.game2.get_game_id())

    def test_game3(self):
        self.assertEqual(self.player_count, self.game3.get_player_count())
        self.assertEqual(self.game3.get_game_id(), self.game3.get_game_id())


class StudInitRaiseError(TestCase):
    def setUp(self):
        self.str_type = 'wrong type'
        self.double_type = 1.1

    def test_raise_type_error(self):
        with self.assertRaises(TypeError):
            FiveCardStud(self.str_type)
        with self.assertRaises(TypeError):
            FiveCardStud(self.double_type)


class StudPlayerCount(TestCase):
    def setUp(self):
        self.game1 = FiveCardStud(0)
        self.game2 = FiveCardStud(0)
        self.game3 = FiveCardStud(0)
        self.game4 = FiveCardStud(0)

        self.player_count1 = 0
        self.player_count2 = 1
        self.player_count3 = 7
        self.player_count4 = 8

        for i in range(1):
            self.game2.add_player(Mock(spec=Player))
        for i in range(7):
            self.game3.add_player(Mock(spec=Player))
        for i in range(8):
            self.game4.add_player(Mock(spec=Player))

    def test_empty_game(self):
        self.assertEqual(self.player_count1, self.game1.get_player_count())

    def test_one_player(self):
        self.assertEqual(self.player_count2, self.game2.get_player_count())

    def test_seven_player(self):
        self.assertEqual(self.player_count3, self.game3.get_player_count())

    def test_eight_player(self):
        self.assertEqual(self.player_count4, self.game4.get_player_count())


class StudGetGameId(TestCase):
    def setUp(self):
        self.game1_id = 0
        self.game2_id = 1
        self.game3_id = -1
        self.game4_id = 7

        self.game1 = FiveCardStud(self.game1_id)
        self.game2 = FiveCardStud(self.game2_id)
        self.game3 = FiveCardStud(self.game3_id)
        self.game4 = FiveCardStud(self.game4_id)

    def test_game1(self):
        self.assertEqual(self.game1.get_game_id(), self.game1.get_game_id())

    def test_game2(self):
        self.assertEqual(self.game2.get_game_id(), self.game2.get_game_id())

    def test_game3(self):
        self.assertEqual(self.game3.get_game_id(), self.game3.get_game_id())

    def test_game4(self):
        self.assertEqual(self.game4.get_game_id(), self.game4.get_game_id())



class StudPlayerJoin(TestCase):
    def setUp(self):
        self.game = FiveCardStud(0)
        self.player1 = Mock(spec=Player)
        self.player2 = Mock(spec=Player)
        self.player3 = Mock(spec=Player)
        self.player4 = Mock(spec=Player)
        self.player5 = Mock(spec=Player)
        self.player6 = Mock(spec=Player)
        self.player7 = Mock(spec=Player)
        self.player8 = Mock(spec=Player)
        self.player9 = Mock(spec=Player)

    def test_one_player(self):
        self.assertEqual(self.player1, self.game.add_player(self.player1))

    def test_seven_player(self):
        self.assertEqual(self.player1, self.game.add_player(self.player1))
        self.assertEqual(self.player2, self.game.add_player(self.player2))
        self.assertEqual(self.player3, self.game.add_player(self.player3))
        self.assertEqual(self.player4, self.game.add_player(self.player4))
        self.assertEqual(self.player5, self.game.add_player(self.player5))
        self.assertEqual(self.player6, self.game.add_player(self.player6))
        self.assertEqual(self.player7, self.game.add_player(self.player7))

    def test_eight_player(self):
        self.assertEqual(self.player1, self.game.add_player(self.player1))
        self.assertEqual(self.player2, self.game.add_player(self.player2))
        self.assertEqual(self.player3, self.game.add_player(self.player3))
        self.assertEqual(self.player4, self.game.add_player(self.player4))
        self.assertEqual(self.player5, self.game.add_player(self.player5))
        self.assertEqual(self.player6, self.game.add_player(self.player6))
        self.assertEqual(self.player7, self.game.add_player(self.player7))
        self.assertEqual(self.player8, self.game.add_player(self.player8))

    def test_add_to_full_game(self):
        self.assertEqual(self.player1, self.game.add_player(self.player1))
        self.assertEqual(self.player2, self.game.add_player(self.player2))
        self.assertEqual(self.player3, self.game.add_player(self.player3))
        self.assertEqual(self.player4, self.game.add_player(self.player4))
        self.assertEqual(self.player5, self.game.add_player(self.player5))
        self.assertEqual(self.player6, self.game.add_player(self.player6))
        self.assertEqual(self.player7, self.game.add_player(self.player7))
        self.assertEqual(self.player8, self.game.add_player(self.player8))
        self.assertEqual(None, self.game.add_player(self.player9))

    def test_add_player_in_game(self):
        self.assertEqual(self.player1, self.game.add_player(self.player1))
        self.assertEqual(None, self.game.add_player(self.player1))


class StudJoinRaiseError(TestCase):
    def setUp(self):
        self.game = FiveCardStud(0)
        self.str_type = 'wrong type'
        self.int_type = 1

    def test_raise_type_error(self):
        with self.assertRaises(TypeError):
            self.game.add_player(self.str_type)
        with self.assertRaises(TypeError):
            self.game.add_player(self.int_type)


class StudRemovePlayer(TestCase):
    def setUp(self):
        self.game1 = FiveCardStud(0)
        self.game2 = FiveCardStud(0)
        self.players = []

        for i in range(8):
            self.players.append(Mock(spec=Player))
        for i in range(2):
            self.game1.add_player(self.players[i])
        for i in range(8):
            self.game2.add_player(self.players[i])

    def test_remove_two_player_game(self):
        self.assertEqual(self.players[1], self.game1.remove_player(self.players[1]))

    def test_remove_eight_player_game(self):
        self.assertEqual(self.players[5], self.game2.remove_player(self.players[5]))

    def test_remove_player_not_in_game(self):
        self.assertEqual(None, self.game2.remove_player(Mock(spec=Player)))


class StudRemovePlayerRaiseError(TestCase):
    def setUp(self):
        self.game = FiveCardStud(0)
        self.str_type = 'wrong type'
        self.int_type = 1

    def test_raise_type_error(self):
        with self.assertRaises(TypeError):
            self.game.remove_player(self.str_type)
        with self.assertRaises(TypeError):
            self.game.remove_player(self.int_type)


class HandTypes(TestCase):
    def card(self, s, r):
        self.mock_card = Mock(spec=Card)
        self.mock_card.getSuit = Mock(return_value=s)
        self.mock_card.getRank = Mock(return_value=r)
        return self.mock_card

    def setUp(self):
        self.game = FiveCardStud(0)

        self.high_card_hands = [
            [self.card(2, 2), self.card(1, 9), self.card(3, 3), self.card(4, 5), self.card(3, 7)],
            [self.card(2, 7), self.card(1, 5), self.card(3, 1), self.card(4, 11), self.card(3, 6)],
            [self.card(4, 2), self.card(2, 3), self.card(2, 4), self.card(2, 5), self.card(2, 7)],
            [self.card(2, 1), self.card(1, 13), self.card(3, 12), self.card(4, 11), self.card(3, 8)]
        ]

        self.pair_hands = [
            # pair in order
            [self.card(2, 2), self.card(1, 2), self.card(3, 3), self.card(4, 5), self.card(3, 7)],
            # pair in random position
            [self.card(2, 2), self.card(1, 1), self.card(3, 3), self.card(4, 5), self.card(3, 1)],
            # pair in random position
            [self.card(4, 3), self.card(1, 2), self.card(3, 3), self.card(4, 13), self.card(3, 12)],
            # pair in random position
            [self.card(2, 9), self.card(1, 2), self.card(3, 2), self.card(4, 5), self.card(3, 7)]
        ]

        self.two_pair_hands = [
            # two pairs in order
            [self.card(2, 2), self.card(1, 2), self.card(3, 7), self.card(4, 7), self.card(3, 5)],
            # space between two pairs
            [self.card(2, 1), self.card(1, 1), self.card(3, 5), self.card(4, 10), self.card(3, 10)],
            # two pairs in random position
            [self.card(2, 3), self.card(1, 2), self.card(3, 3), self.card(4, 2), self.card(3, 12)],
            # two pairs in random position
            [self.card(4, 5), self.card(1, 2), self.card(3, 2), self.card(3, 9), self.card(3, 5)]
        ]

        self.three_of_kind_hands = [
            # two pairs in order
            [self.card(2, 2), self.card(1, 2), self.card(3, 2), self.card(4, 5), self.card(3, 7)],
            # space between two pairs
            [self.card(2, 2), self.card(1, 1), self.card(2, 5), self.card(4, 5), self.card(3, 5)],
            # two pairs in random position
            [self.card(2, 3), self.card(1, 2), self.card(3, 3), self.card(4, 3), self.card(3, 12)],
            # two pairs in random position
            [self.card(2, 9), self.card(1, 9), self.card(3, 4), self.card(4, 5), self.card(3, 9)]
        ]

        self.straight_hands = [
            # simple straight
            [self.card(2, 2), self.card(1, 3), self.card(3, 4), self.card(4, 5), self.card(3, 6)],
            # simple straight
            [self.card(2, 5), self.card(1, 6), self.card(3, 7), self.card(4, 8), self.card(3, 9)],
            # high straight 10-Ace in order
            [self.card(2, 10), self.card(1, 11), self.card(3, 12), self.card(4, 13), self.card(3, 1)],
            # high straight 10-Ace mixed
            [self.card(2, 13), self.card(2, 11), self.card(3, 1), self.card(4, 12), self.card(3, 10)],
            # low straight Ace-5 in order
            [self.card(2, 1), self.card(1, 2), self.card(3, 3), self.card(4, 4), self.card(3, 5)],
            # low straight Ace-5 mixed
            [self.card(2, 3), self.card(1, 1), self.card(3, 5), self.card(4, 4), self.card(3, 2)]
        ]

        self.flush_hands = [
            # flush with all spades
            [self.card(2, 2), self.card(2, 9), self.card(2, 3), self.card(2, 5), self.card(2, 7)],
            # flush with all clubs
            [self.card(1, 7), self.card(1, 5), self.card(1, 1), self.card(1, 11), self.card(1, 6)],
            # flush with all diamonds
            [self.card(4, 2), self.card(4, 3), self.card(4, 4), self.card(4, 5), self.card(4, 7)],
            # flush with all hearts
            [self.card(3, 1), self.card(3, 13), self.card(3, 9), self.card(3, 11), self.card(3, 10)]
        ]

        self.full_house_hands = [
            # three of a kind followed by pair
            [self.card(2, 2), self.card(1, 2), self.card(3, 2), self.card(4, 7), self.card(3, 7)],
            # pair followed by three of a kind
            [self.card(2, 1), self.card(1, 1), self.card(2, 5), self.card(4, 5), self.card(3, 5)],
            # mixed full house
            [self.card(2, 3), self.card(1, 2), self.card(3, 3), self.card(4, 2), self.card(4, 3)],
            # mixed full house
            [self.card(2, 9), self.card(1, 9), self.card(3, 5), self.card(4, 5), self.card(3, 9)]
        ]

        self.four_of_kind_hands = [
            # four of a kind in order at beginning
            [self.card(2, 2), self.card(1, 2), self.card(3, 2), self.card(4, 2), self.card(3, 7)],
            # four of a kind in order at end
            [self.card(2, 2), self.card(1, 5), self.card(2, 5), self.card(4, 5), self.card(3, 5)],
            # mixed four of a kind
            [self.card(2, 3), self.card(1, 3), self.card(3, 4), self.card(4, 3), self.card(3, 3)],
            # mixed four of a kind
            [self.card(2, 9), self.card(1, 5), self.card(3, 9), self.card(4, 9), self.card(1, 9)]
        ]

        self.straight_flush_hands = [
            # straight in order with all spades
            [self.card(2, 2), self.card(2, 3), self.card(2, 4), self.card(2, 5), self.card(2, 6)],
            # mixed straight with all spades
            [self.card(2, 4), self.card(2, 3), self.card(2, 6), self.card(2, 5), self.card(2, 2)],
            # straight in order with all hearts
            [self.card(3, 5), self.card(3, 6), self.card(3, 7), self.card(3, 8), self.card(3, 9)],
            # mixed straight with all hearts
            [self.card(3, 7), self.card(3, 9), self.card(3, 5), self.card(3, 8), self.card(3, 6)],
            # straight in order with all clubs
            [self.card(1, 10), self.card(1, 11), self.card(1, 12), self.card(1, 13), self.card(1, 1)],
            # mixed straight with all clubs
            [self.card(1, 1), self.card(1, 12), self.card(1, 11), self.card(1, 10), self.card(1, 13)],
            # straight in order with all diamonds
            [self.card(4, 1), self.card(4, 2), self.card(4, 3), self.card(4, 4), self.card(4, 5)],
            # mixed straight with all diamonds
            [self.card(4, 2), self.card(4, 5), self.card(4, 1), self.card(4, 3), self.card(4, 4)]
        ]


class StudEvaluateWinners(HandTypes):
    def setUp(self):
        super().setUp()
        self.game_two_player = FiveCardStud(0)
        self.game_five_player = FiveCardStud(0)
        self.game_multiple_winners = FiveCardStud(0)
        self.hand_size = 5

        self.players = []
        hands = [self.high_card_hands[0], self.pair_hands[0], self.two_pair_hands[0],
                 self.three_of_kind_hands[0], self.straight_hands[0]]
        for i in range(5):
            mock_hand = Mock(spec=Hand)
            mock_hand.get_cards = Mock(return_value=hands[i])
            mock_hand.get_hand_score = Mock(return_value=PokerHandTypes(i + 1))
            mock_hand.get_hand_size = Mock(return_value=5)
            mock_player = Mock(spec=Player)
            mock_player.__str__ = Mock(return_value=f'player{str(i)}')
            mock_player.get_hand = Mock(return_value=mock_hand)
            self.players.append(mock_player)

        hand = Mock(spec=Hand)
        hand.get_cards = Mock(return_value=hands[4])
        hand.get_hand_score = Mock(return_value=PokerHandTypes(5))
        player6 = Mock(spec=Player)
        player6.__str__ = Mock(return_value=f'player6')
        player6.get_hand = Mock(return_value=hand)
        self.players.append(player6)

        for i in range(2):
            self.game_two_player.add_player(self.players[i])

        for i in range(5):
            self.game_five_player.add_player(self.players[i])

        for player in self.players:
            self.game_multiple_winners.add_player(player)

        self.winner_two_player_game = [self.players[1]]
        self.winner_five_player_game = [self.players[4]]
        self.multiple_winners = [self.players[4], self.players[5]]

    def test_two_player_one_winner(self):
        self.assertEqual(self.winner_two_player_game, self.game_two_player.evaluate_winners())

    def test_five_player_one_winner(self):
        self.assertEqual(self.winner_five_player_game, self.game_five_player.evaluate_winners())

    def test_multiple_winners(self):
        self.assertEqual(self.multiple_winners, self.game_multiple_winners.evaluate_winners())


class StudEvaluateHand(HandTypes):
    def setUp(self):
        super().setUp()

        self.game = FiveCardStud(0)
        self.hand = Mock(spec=Hand)

    def test_high_card_hand(self):
        self.hand.get_cards = Mock(return_value=self.high_card_hands[0])
        self.assertEqual(PokerHandTypes.HIGH_CARD, self.game.evaluate_hand(self.hand))

    def test_one_pair_hand(self):
        self.hand.get_cards = Mock(return_value=self.pair_hands[0])
        self.assertEqual(PokerHandTypes.ONE_PAIR, self.game.evaluate_hand(self.hand))

    def test_two_pair_hand(self):
        self.hand.get_cards = Mock(return_value=self.two_pair_hands[0])
        self.assertEqual(PokerHandTypes.TWO_PAIR, self.game.evaluate_hand(self.hand))

    def test_three_of_kind_hand(self):
        self.hand.get_cards = Mock(return_value=self.three_of_kind_hands[0])
        self.assertEqual(PokerHandTypes.THREE_OF_KIND, self.game.evaluate_hand(self.hand))

    def test_straight_hand(self):
        self.hand.get_cards = Mock(return_value=self.straight_hands[0])
        self.assertEqual(PokerHandTypes.STRAIGHT, self.game.evaluate_hand(self.hand))

    def test_flush_hand(self):
        self.hand.get_cards = Mock(return_value=self.flush_hands[0])
        self.assertEqual(PokerHandTypes.FLUSH, self.game.evaluate_hand(self.hand))

    def test_full_house_hand(self):
        self.hand.get_cards = Mock(return_value=self.full_house_hands[0])
        self.assertEqual(PokerHandTypes.FULL_HOUSE, self.game.evaluate_hand(self.hand))

    def test_four_of_kind_hand(self):
        self.hand.get_cards = Mock(return_value=self.four_of_kind_hands[0])
        self.assertEqual(PokerHandTypes.FOUR_OF_KIND, self.game.evaluate_hand(self.hand))

    def test_straight_flush_hand(self):
        self.hand.get_cards = Mock(return_value=self.straight_flush_hands[0])
        self.assertEqual(PokerHandTypes.STRAIGHT_FLUSH, self.game.evaluate_hand(self.hand))


class StudEvaluateHandRaiseError(TestCase):
    def setUp(self):
        self.game = FiveCardStud(0)
        self.str_type = 'wrong type'
        self.int_type = 1

    def test_raise_type_error(self):
        with self.assertRaises(TypeError):
            self.game.evaluate_hand(self.str_type)
        with self.assertRaises(TypeError):
            self.game.evaluate_hand(self.int_type)


class StudOnePair(HandTypes):
    def test_one_pair_hands(self):
        for hand in self.pair_hands:
            self.assertTrue(self.game.is_one_pair(hand))
        for hand in self.two_pair_hands:
            self.assertTrue(self.game.is_one_pair(hand))
        for hand in self.three_of_kind_hands:
            self.assertTrue(self.game.is_one_pair(hand))
        for hand in self.full_house_hands:
            self.assertTrue(self.game.is_one_pair(hand))
        for hand in self.four_of_kind_hands:
            self.assertTrue(self.game.is_one_pair(hand))

    def test_non_one_pair_hands(self):
        for hand in self.high_card_hands:
            self.assertFalse(self.game.is_one_pair(hand))
        for hand in self.straight_hands:
            self.assertFalse(self.game.is_one_pair(hand))
        for hand in self.flush_hands:
            self.assertFalse(self.game.is_one_pair(hand))
        for hand in self.straight_flush_hands:
            self.assertFalse(self.game.is_one_pair(hand))


class StudTwoPair(HandTypes):
    def test_two_pair_hands(self):
        for hand in self.two_pair_hands:
            self.assertTrue(self.game.is_two_pair(hand))
        for hand in self.full_house_hands:
            self.assertTrue(self.game.is_two_pair(hand))
        for hand in self.four_of_kind_hands:
            self.assertTrue(self.game.is_two_pair(hand))

    def test_non_two_pair_hands(self):
        for hand in self.high_card_hands:
            self.assertFalse(self.game.is_two_pair(hand))
        for hand in self.pair_hands:
            self.assertFalse(self.game.is_two_pair(hand))
        for hand in self.three_of_kind_hands:
            self.assertFalse(self.game.is_two_pair(hand))
        for hand in self.straight_hands:
            self.assertFalse(self.game.is_two_pair(hand))
        for hand in self.flush_hands:
            self.assertFalse(self.game.is_two_pair(hand))
        for hand in self.straight_flush_hands:
            self.assertFalse(self.game.is_two_pair(hand))


class StudThreeOfKind(HandTypes):
    def test_three_of_kind_hands(self):
        for hand in self.three_of_kind_hands:
            self.assertTrue(self.game.is_three_of_kind(hand))
        for hand in self.full_house_hands:
            self.assertTrue(self.game.is_three_of_kind(hand))
        for hand in self.four_of_kind_hands:
            self.assertTrue(self.game.is_three_of_kind(hand))

    def test_non_three_of_kind_hands(self):
        for hand in self.high_card_hands:
            self.assertFalse(self.game.is_three_of_kind(hand))
        for hand in self.pair_hands:
            self.assertFalse(self.game.is_three_of_kind(hand))
        for hand in self.two_pair_hands:
            self.assertFalse(self.game.is_three_of_kind(hand))
        for hand in self.straight_hands:
            self.assertFalse(self.game.is_three_of_kind(hand))
        for hand in self.flush_hands:
            self.assertFalse(self.game.is_three_of_kind(hand))
        for hand in self.straight_flush_hands:
            self.assertFalse(self.game.is_three_of_kind(hand))


class StudStraight(HandTypes):
    def test_straight_hands(self):
        for hand in self.straight_hands:
            self.assertTrue(self.game.is_straight(hand))
        for hand in self.straight_flush_hands:
            self.assertTrue(self.game.is_straight(hand))

    def test_non_straight_hands(self):
        for hand in self.high_card_hands:
            self.assertFalse(self.game.is_straight(hand))
        for hand in self.pair_hands:
            self.assertFalse(self.game.is_straight(hand))
        for hand in self.two_pair_hands:
            self.assertFalse(self.game.is_straight(hand))
        for hand in self.three_of_kind_hands:
            self.assertFalse(self.game.is_straight(hand))
        for hand in self.flush_hands:
            self.assertFalse(self.game.is_straight(hand))
        for hand in self.full_house_hands:
            self.assertFalse(self.game.is_straight(hand))
        for hand in self.four_of_kind_hands:
            self.assertFalse(self.game.is_straight(hand))


class StudFlush(HandTypes):
    def test_flush_hands(self):
        for hand in self.flush_hands:
            self.assertTrue(self.game.is_flush(hand))
        for hand in self.straight_flush_hands:
            self.assertTrue(self.game.is_flush(hand))

    def test_non_flush_hands(self):
        for hand in self.high_card_hands:
            self.assertFalse(self.game.is_flush(hand))
        for hand in self.pair_hands:
            self.assertFalse(self.game.is_flush(hand))
        for hand in self.two_pair_hands:
            self.assertFalse(self.game.is_flush(hand))
        for hand in self.three_of_kind_hands:
            self.assertFalse(self.game.is_flush(hand))
        for hand in self.straight_hands:
            self.assertFalse(self.game.is_flush(hand))
        for hand in self.full_house_hands:
            self.assertFalse(self.game.is_flush(hand))
        for hand in self.full_house_hands:
            self.assertFalse(self.game.is_flush(hand))


class StudFullHouse(HandTypes):
    def test_full_house_hands(self):
        for hand in self.full_house_hands:
            self.assertTrue(self.game.is_full_house(hand))

    def test_non_full_house_hands(self):
        for hand in self.high_card_hands:
            self.assertFalse(self.game.is_full_house(hand))
        for hand in self.pair_hands:
            self.assertFalse(self.game.is_full_house(hand))
        for hand in self.two_pair_hands:
            self.assertFalse(self.game.is_full_house(hand))
        for hand in self.three_of_kind_hands:
            self.assertFalse(self.game.is_full_house(hand))
        for hand in self.straight_hands:
            self.assertFalse(self.game.is_full_house(hand))
        for hand in self.flush_hands:
            self.assertFalse(self.game.is_full_house(hand))
        for hand in self.four_of_kind_hands:
            self.assertFalse(self.game.is_full_house(hand))
        for hand in self.straight_flush_hands:
            self.assertFalse(self.game.is_full_house(hand))


class StudFourOfKind(HandTypes):
    def test_four_of_kind_hands(self):
        for hand in self.four_of_kind_hands:
            self.assertTrue(self.game.is_four_of_kind(hand))

    def test_non_four_of_kind_hands(self):
        for hand in self.high_card_hands:
            self.assertFalse(self.game.is_four_of_kind(hand))
        for hand in self.pair_hands:
            self.assertFalse(self.game.is_four_of_kind(hand))
        for hand in self.two_pair_hands:
            self.assertFalse(self.game.is_four_of_kind(hand))
        for hand in self.three_of_kind_hands:
            self.assertFalse(self.game.is_four_of_kind(hand))
        for hand in self.straight_hands:
            self.assertFalse(self.game.is_four_of_kind(hand))
        for hand in self.flush_hands:
            self.assertFalse(self.game.is_four_of_kind(hand))
        for hand in self.full_house_hands:
            self.assertFalse(self.game.is_four_of_kind(hand))
        for hand in self.straight_flush_hands:
            self.assertFalse(self.game.is_four_of_kind(hand))


class StudStraightFlush(HandTypes):
    class Straight(HandTypes):
        def test_straight_flush_hands(self):
            for hand in self.straight_flush_hands:
                self.assertTrue(self.game.is_straight_flush(hand))

        def test_non_straight_flush_hands(self):
            for hand in self.high_card_hands:
                self.assertFalse(self.game.is_straight_flush(hand))
            for hand in self.pair_hands:
                self.assertFalse(self.game.is_straight_flush(hand))
            for hand in self.two_pair_hands:
                self.assertFalse(self.game.is_straight_flush(hand))
            for hand in self.three_of_kind_hands:
                self.assertFalse(self.game.is_straight_flush(hand))
            for hand in self.straight_hands:
                self.assertFalse(self.game.is_straight_flush(hand))
            for hand in self.flush_hands:
                self.assertFalse(self.game.is_straight_flush(hand))
            for hand in self.full_house_hands:
                self.assertFalse(self.game.is_straight_flush(hand))
            for hand in self.four_of_kind_hands:
                self.assertFalse(self.game.is_straight_flush(hand))


class StudDealCards(TestCase):
    def setUp(self):
        self.game_two_player = FiveCardStud(0)
        self.game_three_player = FiveCardStud(0)
        self.game_eight_player = FiveCardStud(0)
        self.mock_deck = Mock(spec=Deck)
        self.mock_deck.cards = []
        self.deck_size = 52
        for i in range(self.deck_size):
            self.mock_deck.cards.append(Mock(spec=Card))
        self.mock_deck.draw = Mock(spec=Card)
        self.two_player_deck_count = 42
        self.three_player_deck_count = 37
        self.eight_player_deck_count = 12

        for i in range(2):
            self.game_two_player.add_player(Mock(spec=Player))
        for i in range(3):
            self.game_three_player.add_player(Mock(spec=Player))
        for i in range(8):
            self.game_eight_player.add_player(Mock(spec=Player))

    def test_deal_cards_two_players(self):
        self.game_two_player.deal_cards(self.mock_deck)
        self.assertEqual(self.two_player_deck_count, self.deck_size - self.mock_deck.draw.call_count)

    def test_deal_cards_three_players(self):
        self.game_three_player.deal_cards(self.mock_deck)
        self.assertEqual(self.three_player_deck_count, self.deck_size - self.mock_deck.draw.call_count)

    def test_deal_cards_eight_players(self):
        self.game_eight_player.deal_cards(self.mock_deck)
        self.assertEqual(self.eight_player_deck_count, self.deck_size - self.mock_deck.draw.call_count)


class StudPlayRoundEnoughPlayers(HandTypes):
    def setUp(self):
        super().setUp()
        self.game_two_player = FiveCardStud(0)
        self.game_five_player = FiveCardStud(0)
        self.game_multiple_winners = FiveCardStud(0)
        self.hand_size = 5
        self.initial_hand_score = 0

        self.players = []
        hands = [self.high_card_hands[0], self.pair_hands[0], self.two_pair_hands[0],
                 self.three_of_kind_hands[0], self.straight_hands[0]]
        for i in range(5):
            mock_hand = Mock(spec=Hand)
            mock_hand.get_cards = Mock(return_value=hands[i])
            mock_hand.get_hand_score = Mock(return_value=PokerHandTypes(i + 1))
            mock_hand.get_hand_size = Mock(return_value=5)
            mock_player = Mock(spec=Player)
            mock_player.__str__ = Mock(return_value=f'player{str(i)}')
            mock_player.get_hand = Mock(return_value=mock_hand)
            self.players.append(mock_player)

        hand = Mock(spec=Hand)
        hand.get_cards = Mock(return_value=hands[4])
        hand.get_hand_score = Mock(return_value=PokerHandTypes(5))
        player6 = Mock(spec=Player)
        player6.__str__ = Mock(return_value=f'player6')
        player6.get_hand = Mock(return_value=hand)
        self.players.append(player6)

        for i in range(2):
            self.game_two_player.add_player(self.players[i])

        for i in range(5):
            self.game_five_player.add_player(self.players[i])

        for player in self.players:
            self.game_multiple_winners.add_player(player)

        self.winner_two_player_game = \
            f'winner: {self.players[1].__str__()} ({self.players[1].get_hand().get_hand_score().name})'
        self.winner_five_player_game = \
            f'winner: {self.players[4].__str__()} ({self.players[4].get_hand().get_hand_score().name})'
        self.multiple_winners = \
            f'winner: {self.players[4].__str__()} ({self.players[4].get_hand().get_hand_score().name}) & ' \
            f'{self.players[5].__str__()} ({self.players[5].get_hand().get_hand_score().name})'

    def test_two_player_round(self):
        self.assertEqual(self.winner_two_player_game, self.game_two_player.play_round())
        for i in range(2):
            self.assertEqual(self.hand_size, self.players[i].get_hand().get_hand_size())
            self.assertNotEqual(self.initial_hand_score, self.players[i].get_hand().get_hand_score)

    def test_five_player_round(self):
        self.assertEqual(self.winner_five_player_game, self.game_five_player.play_round())
        for i in range(5):
            self.assertEqual(self.hand_size, self.players[i].get_hand().get_hand_size())
            self.assertNotEqual(self.initial_hand_score, self.players[i].get_hand().get_hand_score)

    def test_round_with_multiple_winners(self):
        self.assertEqual(self.multiple_winners, self.game_multiple_winners.play_round())


class StudPlayRoundNotEnoughPlayers(TestCase):
    def setUp(self):
        self.game_zero_player = FiveCardStud(0)
        self.game_one_player = FiveCardStud(0)

        self.game_one_player.add_player(Mock(spec=Player))
        self.message = 'Need at least two players to play'

    def test_zero_players(self):
        self.assertEqual(self.message, self.game_zero_player.play_round())

    def test_one_player(self):
        self.assertEqual(self.message, self.game_one_player.play_round())
