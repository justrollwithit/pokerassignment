from django.test import TestCase
from pokerApp.models import Table, User


class ModelsTables(TestCase):
    def setUp(self):
        Table.objects.create(name='table1')
        Table.objects.create(name='table2')

        self.table = Table.objects.get(name='table1')
        self.user = User.objects.create(name='user')
        self.user2 = User.objects.create(name='user2')
        self.user3 = User.objects.create(name='user3')
        self.user4 = User.objects.create(name='user4')
        self.user5 = User.objects.create(name='user5')

    def test_add_user(self):
        Table.add_user(self.table, self.user)
        self.assertEqual(self.user, self.table.user_set.all()[0])

    def test_add_user_4_users(self):
        Table.add_user(self.table, self.user)
        Table.add_user(self.table, self.user2)
        Table.add_user(self.table, self.user3)
        Table.add_user(self.table, self.user4)
        self.assertEqual(4, len(self.table.user_set.all()))
        self.assertEqual(self.user, self.table.user_set.filter(name='user')[0])
        self.assertEqual(self.user2, self.table.user_set.filter(name='user2')[0])
        self.assertEqual(self.user3, self.table.user_set.filter(name='user3')[0])
        self.assertEqual(self.user4, self.table.user_set.filter(name='user4')[0])

    def test_remove_user(self):
        Table.add_user(self.table, self.user)
        Table.remove_user(self.table, self.user)
        self.assertEqual(0, len(self.table.user_set.all()))

    def test_remove_4_users(self):
        Table.add_user(self.table, self.user)
        Table.add_user(self.table, self.user2)
        Table.add_user(self.table, self.user3)
        Table.add_user(self.table, self.user4)
        Table.remove_user(self.table, self.user)
        self.assertEqual(3, len(self.table.user_set.all()))
        Table.remove_user(self.table, self.user2)
        self.assertEqual(2, len(self.table.user_set.all()))
        Table.remove_user(self.table, self.user3)
        self.assertEqual(1, len(self.table.user_set.all()))
        self.assertEqual(self.user4, self.table.user_set.filter(name='user4')[0])
        Table.remove_user(self.table, self.user4)
        self.assertEqual(0, len(self.table.user_set.all()))
