from django.test import TestCase
from pokerApp.models import User
from pokerApp.commands.command_runner import CommandRunner

# by Tyler Berray
# As an admin I want to create player accounts so players can play poker.


class CreatePlayerAccount(TestCase):
    def setUp(self):
        self.duplicate_name_message = "Player can't have duplicate names"
        self.admin = "admin"
        self.addUser = "adduser"
        self.user1 = "user1"
        self.user2 = "user2"
        self.createdUserText = "Created user"
        self.onlyAdminCanCreatePlayersMessage = "This command can only be used by the admin"

    # player has username but no password
    def test_player_has_username(self):
        self.assertEqual(f"User {self.user1} was successfully created", CommandRunner.run(self.admin, self.addUser,
                                                                                          [self.user1]))
        try:
            User.objects.get(name=self.user1)  # query database
        except User.DoesNotExist:
            # raises exception when get() fails. if exception -> fail test
            self.assertTrue(False, "test failed because user1 is not in database")

    # can't have duplicate player names
    # assume you will receive some message if you try to add 2 users with same name
    def test_no_duplicate_player_names(self):
        CommandRunner.run(self.admin, self.addUser, [self.user2])
        self.assertEqual(self.duplicate_name_message, CommandRunner.run(self.admin, self.addUser, [self.user2]))

    # only admin can create players
    def test_only_admin_create_players(self):
        # admin create user1
        self.assertEqual(f"User {self.user1} was successfully created", CommandRunner.run(self.admin, self.addUser,
                                                                                   [self.user1]))
        # user1 attempt to create user2, expect error message
        self.assertEqual(self.onlyAdminCanCreatePlayersMessage, CommandRunner.run(self.user1, self.addUser,
                                                                                       [self.user2]))
        # check database. ensure user2 does not exist
        try:
            User.objects.get(name=self.user2)  # query database
        except User.DoesNotExist:
            # test passes if we execute the except part
            self.assertTrue(True, "test passed because user2 is not in the database")


class AdminCannotDoUserCommands(TestCase):
    def setUp(self):
        self.admin = 'admin'
        self.admin_command = 'adduser'
        self.player_command = 'fold'  # Assume this is a valid player command
        self.invalid_command = 'eat'  # A command that does not exist for either players or the admin
        self.adduser_response = 'User player was successfully created'
        self.fold_response = 'folded'
        self.error_response = 'No such command'
        self.command_runner = CommandRunner()

    def test_admin_with_admin_command(self):
        self.assertEqual( self.adduser_response, CommandRunner.run(self.admin, self.admin_command, ['player']))

    def test_admin_with_player_command(self):
        self.assertNotEqual(self.fold_response, CommandRunner.run(self.admin, self.player_command, None))

    def test_admin_with_invalid_command(self):
        self.assertEqual(self.error_response, CommandRunner.run(self.admin, self.invalid_command, None))