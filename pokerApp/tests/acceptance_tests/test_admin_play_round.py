from django.test import TestCase
from pokerApp.commands.command_runner import CommandRunner
from pokerApp.game_objects.gamepool import game_pool, GamePool
from pokerApp.game_objects.player import Player
from pokerApp.models import User


class PlayRoundCommand(TestCase):
    def setUp(self):
        self.admin = 'admin'
        self.player = 'player'
        self.adduser = 'adduser'
        self.playround = 'playround'
        self.message = 'Need at least two players to play'
        self.only_admin_can_play_round = "This command can only be used by the admin"

    def test_play_round_not_enough_players(self):
        game_pool.__init__()
        command_input = CommandRunner.split_command(f'{self.admin}:{self.playround}')
        self.assertEqual(self.message,
                         CommandRunner.run(command_input[0], command_input[1], command_input[2]))

    def test_play_round_enough_players(self):
        for i in range(2):
            game_pool.join_game(Player(User(name=f'player{i}')))
        command_input = CommandRunner.split_command(f'{self.admin}:{self.playround}')
        self.assertNotEqual(self.message,
                            CommandRunner.run(command_input[0], command_input[1], command_input[2]))

    def test_only_admin_can_play_round(self):
        CommandRunner.run(self.admin, self.adduser, [self.player])
        command_input = CommandRunner.split_command(f'{self.player}:{self.playround}')
        self.assertEqual(self.only_admin_can_play_round,
                        CommandRunner.run(command_input[0], command_input[1], command_input[2]))
