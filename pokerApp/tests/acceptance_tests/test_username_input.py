from django.test import TestCase
from pokerApp.models import User
from pokerApp.commands.command_runner import CommandRunner


"""
As a player, I want to be able to enter my username so I can play the poker game.
"""


class NoUsernameInput(TestCase):
    def setUp(self):
        self.valid_player_command = "display" # Assume this is a valid player command
        self.valid_admin_command = "adduser testuser"
        self.invalid_command = "eat" # A command that does not exist for either players or the admin
        self.username = "testuser"
        self.admin = "admin"
        self.response_message = "Please enter a username"
        
    def test_no_command(self):
        self.assertEqual(self.response_message, CommandRunner.run(None, None, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username, None, None))

    def test_no_username_with_player_command(self):
        self.assertEqual(self.response_message, CommandRunner.run(None, self.valid_player_command, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username, self.valid_player_command, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.admin, self.valid_player_command, None))

    def test_no_username_with_valid_admin_command(self):
        self.assertEqual(self.response_message, CommandRunner.run(None, self.valid_admin_command, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username, self.valid_admin_command, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.admin, self.valid_admin_command, None))

    def test_no_username_with_invalid_command(self):
        self.assertEqual(self.response_message, CommandRunner.run(None, self.invalid_command, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username, self.invalid_command, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.admin, self.invalid_command, None))


class UsernameWithWhitespace(TestCase):
    def setUp(self):
        self.valid_command = "display" # Assume this is a valid player command
        self.valid_username = "testuser"
        self.username_left_trailing_space = " testuser1"
        self.username_right_trailing_spaces = "testuser2  "
        self.username_middle_space = "test user"
        self.username_tab = "test\tuser"
        self.username_newline = "test\nuser"

        self.response_message = "Usernames cannot contain whitespace (spaces, tabs, new lines)"

        self.command_runner = CommandRunner()

    def test_username_without_whitespace(self):
        self.assertNotEqual(self.response_message, CommandRunner.run(self.valid_username, None, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.valid_username, self.valid_command, None))

    def test_username_with_left_trailing_space(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.username_left_trailing_space, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username_left_trailing_space, self.valid_command, None))

    def test_username_with_right_trailing_spaces(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.username_right_trailing_spaces, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username_right_trailing_spaces, self.valid_command, None))

    def test_username_with_middle_space(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.username_middle_space, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username_middle_space, self.valid_command, None))

    def test_username_with_tab(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.username_tab, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username_tab, self.valid_command, None))

    def test_username_with_newline(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.username_newline, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username_newline, self.valid_command, None))


class UserExists(TestCase):
    def setUp(self):
        self.username1 = "testuser1"
        self.username2 = "testuser_2"
        self.username3 = "testUser3"
        self.username4 = "testuser4"

        self.valid_command = "display"
        self.invalid_command = "eat"
        self.response_message = "There isn't a user with that username"

        self.command_runner = CommandRunner()

    def createUser(self, username):
        return User.objects.create(name=username)

    def test_no_users(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.username1, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username2, self.valid_command, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username3, self.invalid_command, None))

    def test_one_user_1(self):
        self.createUser(self.username1)
        self.assertEqual(self.response_message, CommandRunner.run(self.username2, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username3, None, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username1, None, None))

    def test_one_user_2(self):
        self.createUser(self.username2)
        self.assertEqual(self.response_message, CommandRunner.run(self.username1, self.valid_command, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username3, self.valid_command, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username2, self.valid_command, None))

    def test_one_user_3(self):
        self.createUser(self.username3)
        self.assertEqual(self.response_message, CommandRunner.run(self.username1, self.invalid_command, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username2, self.invalid_command, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username3, self.invalid_command, None))

    def test_two_users(self):
        self.createUser(self.username1)
        self.createUser(self.username2)
        self.assertEqual(self.response_message, CommandRunner.run(self.username3, None, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username1, None, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username2, None, None))

    def test_three_users(self):
        self.createUser(self.username1)
        self.createUser(self.username2)
        self.createUser(self.username3)

        self.assertEqual(self.response_message, CommandRunner.run(self.username4, None, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username1, None, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username2, None, None))
        self.assertNotEqual(self.response_message, CommandRunner.run(self.username3, None, None))
