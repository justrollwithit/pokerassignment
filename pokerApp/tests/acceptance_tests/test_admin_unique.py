from django.test import TestCase
from pokerApp.commands.command_runner import CommandRunner

''' "As an admin I want to have a special username so I can differentiate from the players." '''


class AdminUnique(TestCase):
    def setUp(self):
        self.admin = 'admin'
        self.player1 = 'player1'
        self.player2 = 'player2'
        self.valid_admin_command = 'adduser'
        self.invalid_admin_command = 'adduser admin'
        self.response_message = 'Created user '

    def test_valid_admin(self):
        self.assertEqual(f"User {self.player2} was successfully created", CommandRunner.run(self.admin, self.valid_admin_command,
                                                                                 ['player2']))
        self.assertNotEqual(self.response_message + self.admin, CommandRunner.run(self.admin,
                                                                                  self.invalid_admin_command, None))

    def test_invalid_admin(self):
        self.assertNotEqual(self.response_message + self.player2, CommandRunner.run(self.player1,
                                                                                    self.valid_admin_command, None))
        self.assertNotEqual(self.response_message + self.admin, CommandRunner.run(self.player1,
                                                                                  self.invalid_admin_command, None))
