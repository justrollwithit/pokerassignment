from django.test import TestCase
from pokerApp.commands.command_runner import CommandRunner
from pokerApp.models import User


class NoCommandInput(TestCase):
    def setUp(self):
        self.username = "user"
        User.create(self.username)
        self.admin = "admin"
        self.args = ["arg"]
        self.response_message = "Please enter a command"

    def test_user_no_command(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.username, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.username, None, self.args))

    def test_admin_no_command(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.admin, None, None))
        self.assertEqual(self.response_message, CommandRunner.run(self.admin, None, self.args))


class RunCommand(TestCase):
    def setUp(self):
        self.admin = "admin"
        self.invalid_command = "command"
        self.valid_command = "adduser"
        self.response_message = "No such command"

    def test_valid_command_recognize(self):
        self.assertNotEqual(self.response_message, CommandRunner.run(self.admin, self.valid_command, None))

    def test_invalid_command_found(self):
        self.assertEqual(self.response_message, CommandRunner.run(self.admin, self.invalid_command, None))
