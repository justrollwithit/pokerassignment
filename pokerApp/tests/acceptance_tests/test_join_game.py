from django.test import TestCase
from pokerApp.commands.command_runner import CommandRunner
from pokerApp.models import User

''' These tests test the PBI stating, As a player I want to join an existing 
    game so I do not have to wait until the current game is over.'''


class NewGame(TestCase):
    def setUp(self):
        self.command = CommandRunner()
        self.join = 'joingame'
        self.message = 'Joined Game'
        self.user1 = User.create("user1")

    def test_new_game(self):
        # assume there are no games currently
        command_input = self.command.split_command(f'{self.user1.name}:{self.join}')
        self.assertEqual(f'{self.user1.name} {self.message}',
                         self.command.run(command_input[0], command_input[1], command_input[2]))


class CurrentGame(TestCase):
    def setUp(self):
        self.message = 'Joined Game'
        self.join = 'joingame'
        self.user1 = User.create("user1")
        self.user2 = User.create("user2")
        self.user3 = User.create("user3")
        self.user4 = User.create("user4")
        self.user5 = User.create("user5")
        self.user6 = User.create("user6")
        self.user7 = User.create("user7")
        self.user8 = User.create("user8")

    def test_ongoing_game(self):
        command_input = CommandRunner.split_command(f'{self.user1.name}:{self.join}')
        self.assertEqual(f'{self.user1.name} {self.message}',
                         CommandRunner.run(command_input[0], command_input[1], command_input[2]))
        # game now started
        for i in range(2, 9):
            command_input = CommandRunner.split_command(f'user{i}:{self.join}')
            self.assertEqual(f'user{i} {self.message}',
                             CommandRunner.run(command_input[0], command_input[1], command_input[2]))


class FullGame(TestCase):
    def setUp(self):
        self.message = 'Joined Game'
        self.join = 'joingame'
        self.user1 = User.create("user1")
        self.user2 = User.create("user2")
        self.user3 = User.create("user3")
        self.user4 = User.create("user4")
        self.user5 = User.create("user5")
        self.user6 = User.create("user6")
        self.user7 = User.create("user7")
        self.user8 = User.create("user8")
        self.user9 = User.create("user9")
        self.user10 = User.create("user10")

    def test_full_game(self):
        command_input = CommandRunner.split_command(f'{self.user1.name}:{self.join}')
        self.assertEqual(f'{self.user1.name} {self.message}',
                         CommandRunner.run(command_input[0], command_input[1], command_input[2]))
        # game started

        for i in range(2, 9):
            command_input = CommandRunner.split_command(f'user{i}:{self.join}')
            self.assertEqual(f'user{i} {self.message}',
                             CommandRunner.run(command_input[0], command_input[1], command_input[2]))
        # full game

        command_input = CommandRunner.split_command(f'user9:{self.join}')
        self.assertEqual(f'{self.user9.name} {self.message}',
                         CommandRunner.run(command_input[0], command_input[1], command_input[2]))

        # now there is a full game and a game of 1
        command_input = CommandRunner.split_command(f'user10:{self.join}')
        self.assertEqual(f'{self.user10.name} {self.message}',
                         CommandRunner.run(command_input[0], command_input[1], command_input[2]))
