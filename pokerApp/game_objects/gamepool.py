from pokerApp.game_objects.five_card_stud import FiveCardStud


class GamePool:
    def __init__(self):
        """
        initializes a list to hold games, with an initial game in there
         and initializes a current_index value to 0"""
        self.game_list = []
        self.game_list.append(FiveCardStud(0))
        self.current_index = 0

    def get_game_list(self):
        """
        returns the list of games field
        :rtype: list
        """
        return self.game_list

    def get_current_index(self):
        """
        returns the current index field
        :rtype: int
        """
        return self.current_index

    def remove_game(self, i):
        """
        if game is in the middle, set that cell to None, otherwise
        remove game at that index and then find the next non-full game
        :param i: index in the list to be removed
        :return: the game that was removed
        """
        retGame = self.game_list[i]
        if i == len(self.game_list) - 1:
            del self.game_list[i]
            self.find_nonempty_game()
        else:
            self.game_list[i] = None
            self.current_index = i
        return retGame

    def join_game(self, p):
        """
        calls game's add_player method on the game list's currentindex game
        checks if that current game is now full, and if it is, set currentIndex again
        :param p: player to added into a game
        :return: the player was added
        """
        retPlayer = self.game_list[self.current_index].add_player(p)
        if self.game_list[self.current_index].get_player_count == 8:
            self.find_nonempty_game()
        return retPlayer


    def find_nonempty_game(self):
        """
        helper method that finds the first non-empty game in the game list
        if there are no empty games, create a new game
        """
        done = False
        for i in range(0, len(self.game_list)):
            if self.game_list[i] != None:
                if self.game_list[i].get_player_count != 8:
                    self.current_index = i
                    done = True
                    break
            else:
                self.create_game(i)
                self.current_index = i
                done = True
                break
        if not done:
            self.create_game(len(self.game_list))
            self.current_index = len(self.game_list) - 1


    def create_game(self, i):
        """
        Helper method that calls game's init class and adds to game list at the passed in index
        """
        if i == len(self.game_list):
            self.game_list.append(FiveCardStud(i))
        else:
            self.game_list[i] = FiveCardStud(i)


game_pool = GamePool()