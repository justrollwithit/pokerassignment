import random

import pokerApp.game_objects.card
from pokerApp.game_objects.card import Card


class Deck:
    def __init__(self):
        # create a standard 52 card deck of cards
        # deck of cards is initialized as 1 - 13 rank all in suit 1, followed by suits 2 - 4
        self.deck = []  # create an empty list to be used as a stack
        for suit in range(1, 5):
            for rank in range(1, 14):
                # append is a stack operation. add each card to "top" of stack
                self.deck.append(Card(suit, rank))

    def count(self):
        # return the number of cards remaining in the deck
        # the count is simply the length of the list "deck"
        return len(self.deck)

    # helper method used to determine if exceptions should be raised in draw and shuffle
    # true if empty. else false
    def is_empty(self):
        # if the length of the list is 0 -> true. otherwise false
        return self.count() == 0

    def draw(self):
        # return and remove the top card in the deck
        # if the deck is empty, raise a ValueError
        if self.is_empty():
            raise ValueError("cannot draw from an empty deck")
        else:
            return self.deck.pop()

    def shuffle(self):
        # shuffle the deck using a random number generator
        if self.is_empty():
            raise TypeError("cannot shuffle an empty deck")
        else:
            random.shuffle(self.deck)
