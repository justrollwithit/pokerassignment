from pokerApp.game_objects.hand import Hand
from pokerApp.models import User


class Player:
    """
    Creates player from an existing user model
    """
    def __init__(self, user):
        if not isinstance(user, User):
            raise TypeError("Player must be a user")
        self.user = user
        self.chips = 100
        self.hand = Hand()

    def get_hand(self):
        """
        Returns a player's hand

        :rtype Hand
        """
        return self.hand

    def bet(self, bet):
        """
        Allows player to make a bet with their chips

        :param bet: amount of chips to bet
        :return: true if player has enough chips to bet and false if player does not have enough chips
        """
        if type(bet) != int:
            raise TypeError
        elif bet <= self.chips:
            self.chips -= bet
            return True
        else:
            return False

    def __str__(self):
        """
        Returns string representation of player

        :return: a string representation of the player containing player's name
        """
        return self.user.name
