from copy import copy
from .deck import Deck
from .game import Game
from .hand import Hand
from .poker_hand_types import PokerHandTypes


class FiveCardStud(Game):
    """
    Intended to define the rules of five card stud poker in order to play a game
    """
    HAND_SIZE = 5
    MULTIPLIER = 15
    DEFAULT = 0
    HIGH_CARD = 1
    ONE_PAIR = 2
    TWO_PAIR = 3
    THREE_OF_KIND = 4
    STRAIGHT = 5
    FLUSH = 6
    FULL_HOUSE = 7
    FOUR_OF_KIND = 8
    STRAIGHT_FLUSH = 9

    def deal_cards(self, deck):
        if not isinstance(deck, Deck):
            raise TypeError('Need a deck to deal cards')
        deck.shuffle()
        for i in range(FiveCardStud.HAND_SIZE):
            for player in self.players:
                player.get_hand().add_card(deck.draw())

    def evaluate_winners(self):
        winners = [self.players[0]]
        winning_hand = self.players[0].get_hand()

        for i in range(1, self.get_player_count()):
            player_hand = self.players[i].get_hand()
            if player_hand.get_hand_score().value > winning_hand.get_hand_score().value:
                winners.clear()
                winning_hand = self.players[i].get_hand()
                winners.append(self.players[i])
            elif player_hand.get_hand_score().value == winning_hand.get_hand_score().value:
                winners.append(self.players[i])

        return winners

    def evaluate_hand(self, hand):
        if not isinstance(hand, Hand):
            raise TypeError('Need a hand to evaluate the cards')
        cards = hand.get_cards()
        hand_score = PokerHandTypes.HIGH_CARD
        if self.is_one_pair(cards):
            hand_score = PokerHandTypes.ONE_PAIR  # self.getScore1Pair(cards)
        if self.is_two_pair(cards):
            hand_score = PokerHandTypes.TWO_PAIR
        if self.is_three_of_kind(cards):
            hand_score = PokerHandTypes.THREE_OF_KIND
        if self.is_straight(cards):
            hand_score = PokerHandTypes.STRAIGHT
        if self.is_flush(cards):
            hand_score = PokerHandTypes.FLUSH
        if self.is_full_house(cards):
            hand_score = PokerHandTypes.FULL_HOUSE
        if self.is_four_of_kind(cards):
            hand_score = PokerHandTypes.FOUR_OF_KIND
        if self.is_straight_flush(cards):
            hand_score = PokerHandTypes.STRAIGHT_FLUSH
        return hand_score

    def is_one_pair(self, cards):
        """
        Helper method for evaluate_hand() to determine if hand has a one pair

        :param cards: cards to be checked for one pair
        :return: True if one pair and false otherwise
        """
        for i in range(len(cards) - 1):
            for j in range(i + 1, len(cards)):
                if cards[i].getRank() == cards[j].getRank():
                    return True
        return False

    def is_two_pair(self, cards):
        """
        Helper method for evaluate_hand() to determine if hand has a two pair

        :param cards: cards to be checked for a two pair
        :return: True if two pair and false otherwise
        """
        clone_hand = copy(cards)
        for i in range(len(clone_hand) - 1):
            for j in range(i + 1, len(clone_hand)):
                if clone_hand[i].getRank() == clone_hand[j].getRank():
                    clone_hand.remove(cards[j])
                    clone_hand.remove(cards[i])
                    return self.is_one_pair(clone_hand)
        return False

    def is_three_of_kind(self, cards):
        """
        Helper method for evaluate_hand() to determine if hand has a three of a kind

        :param cards: cards to be checked for three of a kind
        :return: True if three of a kind and false otherwise
        """
        for i in range(len(cards) - 2):
            for j in range(i + 1, len(cards) - 1):
                for k in range(j + 1, len(cards)):
                    if cards[i].getRank() == cards[j].getRank() and cards[i].getRank() == cards[k].getRank():
                        return True
        return False

    def is_straight(self, cards):
        """
        Helper method for evaluate_hand() to determine if hand has a straight

        :param cards: cards to be checked for straight
        :return: True if one straight and false otherwise
        """
        sorted_hand = sorted(cards, key=lambda card: card.getRank())

        if sorted_hand[0].getRank() == 1 and sorted_hand[1].getRank() == 10 and \
                sorted_hand[2].getRank() == 11 and sorted_hand[3].getRank() == 12 and \
                sorted_hand[4].getRank() == 13:
            return True
        for i in range(len(cards) - 1):
            if sorted_hand[i].getRank() + 1 != sorted_hand[i + 1].getRank():
                return False
        return True

    def is_flush(self, cards):
        """
        Helper method for evaluate_hand() to determine if hand has a flush

        :param cards: cards to be checked for flush
        :return: True if flush and false otherwise
        """
        if cards[0].getSuit() == cards[1].getSuit() and \
                cards[1].getSuit() == cards[2].getSuit() and \
                cards[2].getSuit() == cards[3].getSuit() and \
                cards[3].getSuit() == cards[4].getSuit():
            return True
        return False

    def is_full_house(self, cards):
        """
        Helper method for evaluate_hand() to determine if hand has a full house

        :param cards: cards to be checked for full house
        :return: True if full house and false otherwise
        """
        clone_hand = copy(cards)
        for i in range(len(clone_hand) - 2):
            for j in range(i + 1, len(clone_hand) - 1):
                for k in range(j + 1, len(clone_hand)):
                    if cards[i].getRank() == cards[j].getRank() and cards[i].getRank() == cards[k].getRank():
                        clone_hand.remove(cards[k])
                        clone_hand.remove(cards[j])
                        clone_hand.remove(cards[i])
                        return self.is_one_pair(clone_hand)
        return False

    def is_four_of_kind(self, cards):
        """
        Helper method for evaluate_hand() to determine if hand has a four of a kind

        :param cards: cards to be checked for four or a kind
        :return: True if four of a kind and false otherwise
        """
        for i in range(len(cards) - 3):
            for j in range(i + 1, len(cards) - 2):
                for k in range(j + 1, len(cards) - 1):
                    for l in range(k + 1, len(cards)):
                        if cards[i].getRank() == cards[j].getRank() and cards[i].getRank() == cards[k].getRank() and \
                                cards[i].getRank() == cards[l].getRank():
                            return True
        return False

    def is_straight_flush(self, cards):
        """
        Helper method for evaluate_hand() to determine if hand has a straight flush

        :param cards: cards to be checked for straight flush
        :return: True if straight flush and false otherwise
        """
        return self.is_straight(cards) and self.is_flush(cards)


    # use for high card case (not used yet)
    def getScoreHighCard(self, cards):
        clone_hand = sorted(cards, key=lambda card: card.rank, reverse=True)
        score = 0
        for i in range(len(cards)):
            score += cards[i].getRank()*pow(self.MULTIPLIER, self.HIGH_CARD-i)
        return score

    # use for 1 pair case (not used yet)
    def getScore1Pair(self, cards):
        clone_hand = sorted(cards, key=lambda card: card.getRank(), reverse=True)
        score = 0
        for i in range(len(clone_hand)-1):
            if clone_hand[i].getRank() == clone_hand[i+1].getRank():
                score += clone_hand[i].getRank()*pow(self.MULTIPLIER, self.ONE_PAIR)
                clone_hand.remove(clone_hand[i+1])
                clone_hand.remove(clone_hand[i])
                break
        for i in range(len(clone_hand)):
            score += clone_hand[i].getRank()*pow(self.MULTIPLIER, (self.ONE_PAIR-1)-i)
        return score

    # not used yet
    def getScoreTwoPair(self, cards):
        clone_hand = sorted(cards, key=lambda card: card.rank, reverse=True)
        score = 0
        for i in range(len(clone_hand) - 1):
            if clone_hand[i] == clone_hand[i + 1]:
                score += clone_hand[i].getRank() * pow(self.MULTIPLIER, self.TWO_PAIR)
                clone_hand.remove(clone_hand[i + 1])
                clone_hand.remove(clone_hand[i])
        for i in range(len(clone_hand)-1):
            if clone_hand[i] == clone_hand[i+1]:
                score += clone_hand[i].getRank()*pow(self.MULTIPLIER, self.TWO_PAIR-1)
                clone_hand.remove(clone_hand[i+1])
                clone_hand.remove(clone_hand[i])
        for i in range(len(clone_hand) - 1):
            score += cards[i].getRank() * pow(self.MULTIPLIER, (self.TWO_PAIR-2) - i)

    def play_round(self):
        if self.get_player_count() < 2:
            return 'Need at least two players to play'
        self.deal_cards(Deck())
        for player in self.players:
            hand = player.get_hand()
            hand.set_hand_score(self.evaluate_hand(hand))
        winners = self.evaluate_winners()
        return 'winner: ' + \
               ' & '.join([str(f'{player} ({player.get_hand().get_hand_score().name})') for player in winners])
