from pokerApp.game_objects.player import Player
import abc


class Game(abc.ABC):
    """
    Creates game with an ID number and no players
    """

    FULL_TABLE = 8

    def __init__(self, game_id):
        if not isinstance(game_id, int):
            raise TypeError("Game ID must be an integer")
        self.game_id = game_id
        self.players = []

    def get_player_count(self):
        """
        Returns number of players in the game

        :rtype: int
        """
        return len(self.players)

    def get_game_id(self):
        """
         Returns ID for the game

         :rtype: int
         """
        pass

    def add_player(self, player):
        """
        Adds player to the game

        :param player: player to be added to the game
        :return: player added if added successfully and None if player was already in the game or table is full
        """
        if not isinstance(player, Player):
            raise TypeError('Must be a player to join the game')

        if len(self.players) < Game.FULL_TABLE and player not in self.players:
            self.players.append(player)
            return player
        return None

    def remove_player(self, player):
        """
        Removes player to the game

        :param remove: player to be removed from the game
        :return: player removed if removed successfully and None if player was not in the game
        """
        if not isinstance(player, Player):
            raise TypeError('Can only remove a player from the game')
        if player not in self.players:
            return None
        self.players.remove(player)
        return player


    @abc.abstractmethod
    def deal_cards(self, deck):
        """
        This method tells any derived class to define a way to deal cards in order to play a card game

        :param deck: deck to be used for dealing cards
        :return: remaining deck of cards
        """
        pass

    @abc.abstractmethod
    def evaluate_hand(self, hand):
        """
        This method tells any derived class to define rules for the cards in a player's hand

        :param hand: deck to be used for dealing cards
        :rtype: PokerHandTypes
        """
        pass

    @abc.abstractmethod
    def evaluate_winners(self):
        """
        This method tells any derived class to define a winner based on a player's hand

        :return: a list of players that won the current round
        """
        pass

    @abc.abstractmethod
    def play_round(self):
        """
        This method tells any derived class to define a the rules of playing a round

        :return: a string of the winning players and their hand type or an string stating the round was not played
        """
        pass


