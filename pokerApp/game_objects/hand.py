from pokerApp.game_objects.card import Card
import copy
from pokerApp.game_objects.poker_hand_types import PokerHandTypes


class Hand:
    """
    Sets up a generic card hand.
    """
    def __init__(self):
        self.cards = []
        self.hand_score = PokerHandTypes.DEFAULT


    def add_card(self, card):
        """
        Adds card to hand

        :param card: card to be added to hand
        :type card: card
        :rtype Card
        """
        if not isinstance(card, Card):
            raise TypeError("hand must consist of cards")
        self.cards.append(card)
        return card

    def get_hand_score(self):
        """
        Returns enumeration type of hands score

        :rtype PokerHandTypes
        """
        return self.hand_score

    def set_hand_score(self, hand_score):
        """
        Sets hands score to enumeration type

        :param hand_score: new hand score of the hand
        :type card: PokerHandTypes
        """
        self.hand_score = hand_score

    def get_hand_size(self):
        """
        Returns number of cards in hand

        :rtype int
        """
        return len(self.cards)

    def get_cards(self):
        """
        Returns list of cards in hand

        :return: a list of cards in the hand
        """
        return copy.deepcopy(self.cards)

    def __str__(self):
        """
        Returns string representation of hand

        :return: a string representation of the hand containing all the cards in the hand
        """
        return ' '.join([str(card) for card in self.cards])
