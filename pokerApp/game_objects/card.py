class Card:
    def __init__(self, s, r):
        # initialize a card to the given suit (1-4) and rank (1-13)
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # raise an exception for invalid argument, ValueError

        # Checks for invalid parameter types
        if type(s) != int or type(r) != int:
            raise TypeError("Card must have ints for both rank and suit")

        # Checks for invalid suit values
        if s < 1 or s > 4:
            raise ValueError("Card must have a suit between 1 and 4")

        # Checks for invalid rank values
        if r < 1 or r > 13:
            raise ValueError("Card must have a rank between 1 and 13")

        # Creates instance variables for rank and suit
        self.suit = s
        self.rank = r
        self.value = 0

    def getSuit(self):
        # return the suit of the card (1-4)
        return self.suit

    def getRank(self):
        # return the rank of the card (1-13)
        return self.rank

    def getValue(self):
        # get the game-specific value of a Card
        # if not used, or game is not defined, always returns 0.
        return self.value

    def getRankString(self):
        # Helper function to convert rank from its internal int representation to its external string representation

        # Special ranks (not represented by a number)
        rank_dict = {1: "A", 11: "J", 12: "Q", 13: "K"}

        if 1 < self.getRank() < 11: # Non special ranks
            return f"{self.getRank()}"
        else: # Special ranks
            return f"{rank_dict[self.getRank()]}"

    def getSuitString(self):
        # Helper function to convert rank from its internal int representation to its external string representation
        suit_dict = {1: "C", 2: "S", 3: "H", 4: "D"}
        return suit_dict[self.getSuit()]

    def __str__(self):
        # return rank and suite, as AS for ace of spades, or 3H for
        # three of hearts, JC for jack of clubs.
        return self.getRankString() + self.getSuitString()

    def __eq__(self, other):
        """
        :type other: Card
        """
        return self.rank == other.getRank() and self.suit == other.getSuit() and self.value == other.getValue()

    def __ne__(self, other):
        """
        :type other: Card
        """
        return not self.__eq__(other)