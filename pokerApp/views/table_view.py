from django.shortcuts import render, redirect
from django import views
from pokerApp.models import Table, User


class TableView(views.View):
    def get(self, request):
        # Get needed data from the current session
        username = request.session.get("username")
        table_name = request.session.get("table")

        players = []  # List of Users at the Table, if a Table is found with matching name
        hand = None  # Initialize the current User's hand to be empty, in case there is no current User

        table = Table.objects.filter(name=table_name)  # Query the database for a Table with the same name as table_name

        # Executed if a Table is found
        if table.count() > 0:
            table = table[0]  # Use the first Table returned from the database
            players = [player.name for player in table.user_set.all()]  # Gets the Users at the Table from the database

        full_seats = len(players)  # Get the number of Users at the table

        # Execute if the current session has a User
        if username:
            hand = ["AH", "3S", "9D", "JH", "4D"]  # Set the User's hand
            players.remove(username)  # Remove the current User from players, so it is treated differently in later code
            
        return render(request, 'pokerApp/table.html', {"table_name": table_name, "username": username,
                                                       "players": players, "hand": hand,
                                                       "full_seats": full_seats})

    def post(self, request):
        action = request.POST["action"]
        if action and action == "quit":
            return self.quit(request)

    def quit(self, request):
        table_name = request.session.get("table")
        table = Table.objects.filter(name=table_name)[0]
        username = request.session.pop("username", None)
        user = User.objects.filter(name=username)
        Table.remove_user(table, user[0])
        return redirect("table_list")
