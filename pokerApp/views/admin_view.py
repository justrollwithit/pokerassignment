from django.shortcuts import render
from django import views
from pokerApp.models import User
from pokerApp.models import Table


# Create your views here.
class AdminView(views.View):
    def get(self, request):
        return render(request, 'pokerApp/admin.html')

    def post(self, request):
        if 'user' in self.request.POST:
            #adding a user
            username = request.POST['user']
            password = request.POST['password']
            user = User.objects.filter(name=username)

            if user.count() == 0:
                User.objects.create(name=username, password=password)
                return render(request, 'pokerApp/admin.html', {'message': username+' created successfully'})
            else:
                return render(request, 'pokerApp/admin.html', {'message': username+' already exists'})
        else:
            action = request.POST["tablemode"]
            if action == "Add table":
                #adding a table
                tablename = request.POST['tablename']
                table = Table.objects.filter(name=tablename)

                if table.count() == 0:
                    Table.objects.create(name=tablename)
                    return render(request, 'pokerApp/admin.html', {'message2': tablename+' created successfully'})
                else:
                    return render(request, 'pokerApp/admin.html', {'message2': tablename+' already exists'})
            else:
                #removing
                tablename = request.POST['tablename']
                table = Table.objects.filter(name=tablename)
                if table.count() != 0:
                    toRemove = Table.objects.get(name=tablename)
                    toRemove.delete()
                    return render(request, 'pokerApp/admin.html', {'message2': tablename+' removed successfully'})
                else:
                    return render(request, 'pokerApp/admin.html', {'message2': tablename+' does not exist'})
        

