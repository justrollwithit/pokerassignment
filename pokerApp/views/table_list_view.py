from django.shortcuts import render, redirect
from django import views
from pokerApp.models import User, Table


class TableListView(views.View):
    """
    Class to show a list of tables a registered user can join
    """

    def get(self, request):
        """
        Get request for table list view
        :param request: HTTPRequest object containing data about the request
        :return: table view if user current has session and table list view otherwise
        """
        if request.session.get('username'):
            return redirect('table')

        tables = Table.objects.all()
        return render(request, 'pokerApp/table_list.html', {'tables': tables})

    def post(self, request):
        """
        Post request for table list view

        :param request: HTTPRequest object containing data about the request
        :return: table view if valid user and table list view otherwise
        """
        username = request.POST['username']
        password = request.POST['password']
        user = User.objects.filter(name=username, password=password)
        tables = Table.objects.all()
        if user.count() == 0:
            return render(request, 'pokerApp/table_list.html', {'tables': tables,
                                                                'error_message': 'Invalid username or password'})
        for table in tables:
            if request.POST.get(table.name):
                request.session['username'] = username
                request.session['table'] = table.name
                Table.add_user(table, user[0])

        return redirect('table')
