from django.db import models


class Table(models.Model):
    name = models.CharField(max_length=256)
    is_full = models.BooleanField(default=False)
    MAX_USERS_PER_TABLE = 4

    # create a table with a name supplied by table_name

    # add a user to the table. Throws ValueError if user has not been created already
    @staticmethod
    def add_user(table, user):
        if table.user_set.count() < 4:
            table.user_set.add(user)
            if table.user_set.count() == 4:
                table.is_full = True
            table.save()
            return True
        return False

    # remove a user from the table
    @staticmethod
    def remove_user(table, user):
        if table.user_set.filter(name=user.name):
            return_user = table.user_set.remove(user)
            if table.user_set.count() < 4:
                table.is_full = False
            table.save()
            return return_user
        return None


class User(models.Model):
    name = models.CharField(max_length=256)
    password = models.CharField(max_length=256)
    high_score = models.IntegerField(default=100)
    table = models.ForeignKey(Table, null=True, on_delete=models.CASCADE)

    @staticmethod
    def create(name):
        n = User.objects.create(name=name)
        n.save()
        return n

    @staticmethod
    def exists(username):
        """
        Checks if a User with a given username exists

        :param username: the username of a potential User
        :type username: str
        :return: if the username is used by an existing User
        :rtype: bool
        """
        try:
            User.objects.get(name=username)
            return True
        except User.DoesNotExist:
            return False







